<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index()
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		
		$database 	= $this->db->database;
		$user 		= $this->db->username;
		$pass 		= $this->db->password;
		$host 		= $this->db->hostname;
		
		$dir = dirname(__FILE__) . "/../../db/".DATE('Y-m-d_h-i-s').".sql";
		echo "<h3>Backing up database to `<code>{$dir}</code>`</h3>";
		exec("mysqldump --user={$user} --password={$pass} --host={$host} {$database} --result-file={$dir} 2>&1", $output);
		debug_array("mysqldump --user={$user} --password={$pass} --host={$host} {$database} --result-file={$dir} 2>&1");
	}
}