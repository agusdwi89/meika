<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->db->get_where('_subdomain',array('subdomain'=>subdomain()))->num_rows() == 0) {
			show_404();
		 }
	}

	public function index()
	{
		$this->counting();
		$all_list = [];
		$list = $this->db->get_where('view_list_section',array('subdomain'=>subdomain()));

		foreach ($list->result() as $key) {
			if ($key->db_table == "news") 
				$ls = $this->db->limit(1)->get($key->db_table);
			else $ls = $this->db->get_where($key->db_table,array('id' => $key->section_id));
			
			if ($ls->num_rows == 1) {
				$all_list[] = array(
					'layout'	=> $key->db_table,
					'data'		=> $ls->result()[0]
				);
			}
		}

		$data['list_section']	=	$all_list;
		$this->load->view('v_home',$data);
	}

	function submit_reservation(){
		$data = $this->input->post();
		$data['date'] = format_date_time2(str_replace("/", "-", $data['date']),false);
		
		$this->email('reservation',$data);
		$this->email('reservation',$data, true);
		
		$this->db->insert('form_reservation',$data);
		echo "success";
	}

	function submit_contact(){
		$data = $this->input->post();

		$this->email('contact_us',$data);
		$this->email('contact_us',$data, true);

		$this->db->insert('form_contact',$data);
		echo "success";
	}
	

	private function email($state,$item,$notif = false){
		$EMAIL_ADMIN 	= "canoopix@gmail.com";
		$EMAIL_USER 	= "agusdwi89@gmail.com";
		$EMAIL_PWD 		= "imamSyafii57";

		$item['notif'] = $notif;

		if ($state == "reservation") {
			$d = $this->load->view('v_email',$item,true);
		}else $d = $this->load->view('v_email_contact',$item,true);

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => $EMAIL_USER,
			'smtp_pass' => $EMAIL_PWD,
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
			);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('shopbay.id@gmail.com', 'Meikaclinic.com');
		
		if ($notif) {
			$this->email->to($EMAIL_ADMIN);
		}else{
			$this->email->to($item['email']);
		}

		if ($state == "reservation") {
			$this->email->subject("Reservasi online meika clinic [ ".pretty_date($item['date'],false)."] ");
		}else $this->email->subject("Pertanyaan pengguna meika clinic [ ".pretty_date(date("Y-m-d"),false)."] ");

		$this->email->message($d);  

		$this->email->send();

		echo $this->email->print_debugger();
	}

	function success($unq){
		$data['db']			= $this->db->get_where('orders',array('link_unique'=>$unq));
		$this->load->view('v_order_finish',$data);
	}

	function counting(){
		$data['ip'] 		= $this->input->ip_address();
		$data['subdomain'] 	= subdomain();
		$this->db->insert('visitor',$data);
	}
}