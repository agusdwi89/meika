<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "customer";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	function get_map_product_domain(){
		$list_section = $this->db->get_where('list_section',array('type_sec_id'=>6));
		foreach ($list_section->result() as $k) {
			$section_ids[$k->section_id] = $k;	
		}
		
		$list_product = $this->db->get_where('section_product_items',array('active'=>'yes'));
		foreach ($list_product->result() as $v) {
			$product_ids[$v->id]	= array(
				'data'			=> $v,
				'sd'			=> $section_ids[$v->prd_id]
			);
		}
		return $product_ids;
	}


	public function index()
	{
		$data['local_view'] 			= 'v_customer';
		$data['db'] 					= $this->get_user();
		$data['map_product_domain'] 	= $this->get_map_product_domain();
		$this->load->view('v_manage',$data);
	}

	function detail($id){
		$data['local_view'] 			= 'v_customer_detail';
		$data['cust'] 					= $this->db->get_where('v_manage_order',array('id'=>$id))->row();
		$data['db']						= $this->db->get_where('v_manage_order',array('cust_email'=>$data['cust']->cust_email));
		$data['map_product_domain'] 	= $this->get_map_product_domain();
		$this->load->view('v_manage',$data);	
	}

	function get_user(){
		$sql = "select * from v_manage_order where id in (select MAX(id) from v_manage_order GROUP BY CONCAT(cust_email,prd_id) ORDER BY MAX(id) desc)";
		$sql = $this->db->query($sql);

		if ($this->sub_domain == "all") {
			return $sql->result();
		} else {
			$section = $this->db->query("select id from section_product_items where prd_id in (select section_id from list_section WHERE subdomain = '{$this->sub_domain}' and type_sec_id = 6)");$scarr = array();
			foreach ($section->result() as $s)$scarr[] = $s->id;
			$fa = array();
			foreach ($sql->result() as $k) if (in_array($k->prd_id, $scarr)) $fa[] = $k;
			return $fa;
		}
	}

	function download(){

		$ship					= $this->get_user();
		$map_product_domain 	= $this->get_map_product_domain();
		$data[] 				= array("No","Sub-Domain","Name","Email","Phone","Province","City");

		$i=0;foreach ($ship as $v) {$i++;
			$data[] = array(
				$i,
				$map_product_domain[$v->prd_id]['sd']->subdomain,
				$v->cust_f_name,
				$v->cust_email,
				$v->cust_phone,
				$v->province,
				$v->city
			);
		}

		$date = date_create();
		$d = date_timestamp_get($date);
		$list = $data;
		$fp = fopen("assets/csv/$d.csv", 'w');
		foreach ($list as $fields) {
			fputcsv($fp, $fields);
		}
		fclose($fp);

		$file_name = "customer-$d.csv";
		$file_url = base_url()."assets/csv/$d.csv";
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary"); 
		header("Content-disposition: attachment; filename=\"".$file_name."\""); 
		readfile($file_url);
		exit;
	}
}