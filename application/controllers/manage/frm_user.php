<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frm_user extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function reservation()
	{
		$this->current_menu = "frm_reservation";
		$data['local_view'] = 'v_frm_reservation';
		$data['db'] 		= $this->db->order_by('id','DESC')->get('form_reservation');
		$this->load->view('v_manage',$data);
	}

	public function contact()
	{
		$this->current_menu = "frm_contact";
		$data['local_view'] = 'v_frm_contact';
		$data['db'] 		= $this->db->order_by('id','DESC')->get('form_contact');
		$this->load->view('v_manage',$data);
	}
}