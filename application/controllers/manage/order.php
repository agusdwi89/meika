<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "order";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function index()
	{
		$data['local_view'] 			= 'v_order';
		
		$this->load->view('v_manage',$data);
	}

	function qbu($i){
		return "$i >= '{$this->cdate[0]}' and $i <= DATE_ADD('{$this->cdate[1]}', INTERVAL 1 DAY)";
	}

	function load($date=""){

		$this->cdate = ($date == "")? "" : explode("s", $date);

		if ($this->sub_domain == "all")
			$data['db'] 		= $this->db->query("select * from v_manage_order where {$this->qbu('time_order')}")->result();
		else
			$data['db'] 		= $this->db->query("select * from v_manage_order where subdomain ='{$this->sub_domain}' and {$this->qbu('time_order')}")->result();

		$this->load->view('manage/v_order_load',$data);
	}

	function detail($id){
		$data['local_view'] = 'v_invoice';
		$data['db'] 		= $this->db->get_where('v_manage_order',array('id'=>$id))->row();
		$data['source_id'] 	= $id;
		$this->load->view('v_manage',$data);	
	}

	function print($id){
		$data['db'] 		= $this->db->get_where('v_manage_order',array('id'=>$id))->row();
		$data['source_id'] 	= $id;
		$this->load->view('v_print',$data);	
	}

	function delete($id){
		$this->db->delete('orders', array('id' => $id)); 
		$this->session->set_flashdata('message','Data order has been deleted');
		redirect(base_url("manage/order"));		
	}

	function set_payment($id){
		$payment = $this->db->get_where('orders',array('id'=>$id))->row()->payment;

		$data = array('order_status' =>  'payment-success');
		$this->db->where('id', $id);
		$this->db->update('orders', $data); 

		if ($payment == "cod")
			$this->email_update($id,"order-confirmed");	
		else 
			$this->email_update($id,"payment-success");

		$this->session->set_flashdata('message','Status order has been updated');
		redirect(base_url("manage/order/detail/$id"));
	}

	function set_deliver($id){
		$data = array(
					'order_status' 	=>  'delivery-process',
					'resi'			=> '-'
				);
		$this->db->where('id', $id);
		$this->db->update('orders', $data); 
		
		$this->email_update($id,"delivery-process");
		
		$this->session->set_flashdata('message','Status order has been updated');
		redirect(base_url("manage/order/detail/$id"));
	}

	function set_completed($id){
		$data = array('order_status' =>  'completed');
		$this->db->where('id', $id);
		$this->db->update('orders', $data); 
		
		$this->email_update($id,"completed");
		
		$this->session->set_flashdata('message','Status order has been updated');
		redirect(base_url("manage/order/detail/$id"));
	}

	function resi($id){
		$input = $this->input->post('resi');
		$data = array('resi' => $input  );
		$this->db->where('id', $id);
		$this->db->update('orders', $data); 
		$this->email_update($id,"resi");
	}

	function email_update($id,$state){
		
		$data['db'] = $this->db->get_where('v_manage_order',array('id'=>$id))->row();
		$email = $data['db']->cust_email;
		$data['email_state'] = $state;

		$subject = "";

		switch ($state) {
			case 'payment-success':
				$subject = "Pembayaran Sukses - ".$data['db']->prd_title;
				break;
			case 'order-confirmed':
				$subject = "Pesanan Terkonfirmasi - ".$data['db']->prd_title;
				break;
			case 'delivery-process':
				$subject = "Pesanan Dalam Pengiriman - ".$data['db']->prd_title;
				break;
			case 'resi':
				$subject = "Update Resi Pengiriman - ".$data['db']->prd_title;
				break;
			default:
				$subject = "Pesanan Diterima - ".$data['db']->prd_title;
				break;
		}

		$d = $this->load->view('v_email',$data,true);

		// test email
		// $this->load->view('v_email',$data);
		// return true;

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'shopbay.id',
			'smtp_pass' => 'Zoomy123456!',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('shopbay.id@gmail.com', 'Shop Bay Indonesia');
		$this->email->to($email); 

		$this->email->subject($subject);
		$this->email->message($d);  

		$this->email->send();	
	}

	function download(){

		$ship			=$this->db->get('v_manage_order');
		$account 		= "SHOPBAY.ID";
		$country 		= "IDN";
		$data[] 		= array("No","Account","Order Code","Product Code","Product Name","Customer Name","Mobile-Phone","Email","Country",
								"Province","City","Area","Zip Code","Address","Order Quantity","Logistic Company",
								"Tracking Code","Shipping Cost","Price","Coupon Code","Value Coupon","Payment Method","Total","Status","Date");

		$i=0;foreach ($ship->result() as $v) {$i++;
			$data[] = array(
							$i,
							$account,
							"#SB1".sprintf('%05d', $v->id),
							$v->code,
							$v->prd_title,
							$v->cust_f_name,
							$v->cust_phone,
							$v->cust_email,
							$country,
							$v->province,
							$v->city,
							"Kec. ".$v->cust_kecamatan,
							$v->cust_zip,
							$v->cust_address,
							"1",
							$v->cust_shipping,
							$v->resi,
							$v->prd_shipment,
							$v->prd_price,
							$v->prd_coupon_code,
							$v->prd_coupon,
							$v->payment,
							$v->prd_total,
							$v->order_status,
							format_date_time($v->time_order)
						);
		}

		$date = date_create();
		$d = date_timestamp_get($date);
		$list = $data;
		$fp = fopen("assets/csv/$d.csv", 'w');
		foreach ($list as $fields) {
			fputcsv($fp, $fields);
		}
		fclose($fp);

		$file_name = "order-$d.csv";
		$file_url = base_url()."assets/csv/$d.csv";
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary"); 
		header("Content-disposition: attachment; filename=\"".$file_name."\""); 
		readfile($file_url);
		exit;
	}

	function batch(){
		$data['date'] = $this->input->post();
		$this->load->view('manage/v_order_batch_load',$data);		
	}

	function batch_download($date=""){
		$this->cdate = ($date == "")? "" : explode("s", $date);

		$ship			= $this->db->query("select * from v_manage_order where subdomain ='{$this->sub_domain}' and {$this->qbu('time_order')}");
		$data[] 		= array("type status (follow the rule) : ");
		$data[] 		= array("Code => COD --- others payment method");
		$data[] 		= array("1 => Menunggu Konfirmasi --- Menunggu Pembayaran");
		$data[] 		= array("2 => Konfirmasi Pesanan --- Pembayaran Sukses");
		$data[] 		= array("3 => Proses Pengiriman --- Proses Pengiriman");
		$data[] 		= array("4 => Pembayaran Diterima & Pengiriman Selesai --- Pesanan Diterima");
		$data[] 		= array("     ");
		$data[] 		= array("No","Subdomain","ID","Order Code","Email","Date","Status");

		$i=0;foreach ($ship->result() as $v) {$i++;
			$data[] = array(
							$i,
							$v->subdomain,
							$v->id,
							"#SB1".sprintf('%05d', $v->id),
							$v->cust_email,
							format_date_time($v->time_order),
							$this->numbering_status($v->order_status)
						);
		}

		$date = date_create();
		$d = date_timestamp_get($date)."_batch";
		$list = $data;
		$fp = fopen("assets/csv/$d.csv", 'w');
		foreach ($list as $fields) {
			fputcsv($fp, $fields);
		}
		fclose($fp);

		$file_name = "order-$d.csv";
		$file_url = base_url()."assets/csv/$d.csv";
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary"); 
		header("Content-disposition: attachment; filename=\"".$file_name."\""); 
		readfile($file_url);
		exit;	
	}

	function numbering_status($status){
		switch ($status) {
			case 'wait-payment':
				return 1;break;
			case 'payment-success':
				return 2;break;
			case 'delivery-process':
				return 3;break;
			default:
				return 4;break;
		}
	}

	function lettering_status($status){
		switch ($status) {
			case 1:
				return 'wait-payment';break;
			case 2:
				return 'payment-success';break;
			case 3:
				return 'delivery-process';break;
			default:
				return 'completed';break;
		}
	}

	function batch_upload(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']			= 'assets/csv_batch/';
		$config['allowed_types']		= '*';
		$config['encrypt_name']			= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			debug_array($this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			$name = 'assets/csv_batch/'.$a['file_name']; 

			$status = array(1,2,3,4);

			$csv = array_map('str_getcsv', file($name));

			foreach ($csv as $v) {
				if (in_array($v[6], $status)){
					$data = array('order_status' => $this->lettering_status($v[6]));
					$this->db->where('id', $v[2]);
					$this->db->update('orders', $data); 
				}
			}
		}
		$this->session->set_flashdata('message','batch update finish, please check the data');
		redirect(base_url('manage/order'));
	}
}