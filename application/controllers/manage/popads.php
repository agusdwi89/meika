<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Popads extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "popads";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	function index(){
		if (is_post()) {
			if ($_FILES['userfile']['size'] > 0 ){
				$upload = $this->upload();
				if (!$upload[0]){
					$this->session->set_flashdata('log',$upload[1]);
					$this->session->set_flashdata('message','Error while uploading image');
					redirect(base_url("manage/popads"));
				}else{
					$item['value'] = $upload[1]['file_name'];
					$this->db->where('name', 'pop-ads');
					$this->db->where('subdomain',$this->sub_domain);
					$this->db->update('site_config', $item); 
				}	
			}
			$this->session->set_flashdata('message','Data Saved Successfully');
			redirect(base_url("manage/popads"));
		}
		$data['items'] 		= $this->db->get_where('site_config',array('name'=>'pop-ads','subdomain'=>$this->sub_domain))->row();
		$data['local_view'] = 'v_popads';
		$this->load->view('v_manage',$data);
	}

	function upload(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'assets/popads';
		$config['allowed_types']	= 'jpg|png';
		$config['max_size']			= '10000';
		$config['max_width']		= '5000';
		$config['max_height']		= '5000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			return array(true,$a);
		}
	}

	function delete(){
		$data = array('value' => '');
		$this->db->where('name', 'pop-ads');
		$this->db->where('subdomain',$this->sub_domain);
		$this->db->update('site_config', $data); 

		$this->session->set_flashdata('message','Data Deleted Successfully');
		redirect(base_url("manage/popads"));		
	}
}