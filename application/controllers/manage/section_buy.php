<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section_buy extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "section";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	function edit($id){
		if (is_post()) {

			$item = $this->input->post('ar');
			$link = $this->input->post('state');
			$item['button_link']	= $link[$item['button_type']];

			$this->db->where('id', $id);
			$this->db->update('section_buy', $item); 
			$this->session->set_flashdata('message','Data Saved Successfully');
			redirect(base_url("manage/section_buy/edit/$id"));
		}
		$data['items'] 		= $this->db->get_where('section_buy',array('id'=>$id))->row();
		$data['sectionopt'] = $this->db->get_where('v_section_name',array('subdomain'=>$this->sub_domain));
		$data['local_view'] = 'v_section_buy';
		$this->load->view('v_manage',$data);
	}
}