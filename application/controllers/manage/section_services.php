<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section_services extends CI_Controller {

	var $iconset = array();

	function __construct(){
		parent::__construct();
		$this->iconset = array("icon-user","icon-people","icon-user-female","icon-user-follow","icon-user-following","icon-user-unfollow","icon-login","icon-logout","icon-emotsmile","icon-phone","icon-call-end","icon-call-in","icon-call-out","icon-map","icon-location-pin","icon-direction","icon-directions","icon-compass","icon-layers","icon-menu","icon-list","icon-options-vertical","icon-options","icon-arrow-down","icon-arrow-left","icon-arrow-right","icon-arrow-up","icon-arrow-up-circle","icon-arrow-left-circle","icon-arrow-right-circle","icon-arrow-down-circle","icon-check","icon-clock","icon-plus","icon-minus","icon-close","icon-event","icon-exclamation","icon-organization","icon-trophy","icon-screen-smartphone","icon-screen-desktop","icon-plane","icon-notebook","icon-mustache","icon-mouse","icon-magnet","icon-energy","icon-disc","icon-cursor","icon-cursor-move","icon-crop","icon-chemistry","icon-speedometer","icon-shield","icon-screen-tablet","icon-magic-wand","icon-hourglass","icon-graduation","icon-ghost","icon-game-controller","icon-fire","icon-eyeglass","icon-envelope-open","icon-envelope-letter","icon-bell","icon-badge","icon-anchor","icon-wallet","icon-vector","icon-speech","icon-puzzle","icon-printer","icon-present","icon-playlist","icon-pin","icon-picture","icon-handbag","icon-globe-alt","icon-globe","icon-folder-alt","icon-folder","icon-film","icon-feed","icon-drop","icon-drawer","icon-docs","icon-doc","icon-diamond","icon-cup","icon-calculator","icon-bubbles","icon-briefcase","icon-book-open","icon-basket-loaded","icon-basket","icon-bag","icon-action-undo","icon-action-redo","icon-wrench","icon-umbrella","icon-trash","icon-tag","icon-support","icon-frame","icon-size-fullscreen","icon-size-actual","icon-shuffle","icon-share-alt","icon-share","icon-rocket","icon-question","icon-pie-chart","icon-pencil","icon-note","icon-loop","icon-home","icon-grid","icon-graph","icon-microphone","icon-music-tone-alt","icon-music-tone","icon-earphones-alt","icon-earphones","icon-equalizer","icon-like","icon-dislike","icon-control-start","icon-control-rewind","icon-control-play","icon-control-pause","icon-control-forward","icon-control-end","icon-volume-1","icon-volume-2","icon-volume-off","icon-calendar","icon-bulb","icon-chart","icon-ban","icon-bubble","icon-camrecorder","icon-camera","icon-cloud-download","icon-cloud-upload","icon-envelope","icon-eye","icon-flag","icon-heart","icon-info","icon-key","icon-link","icon-lock","icon-lock-open","icon-magnifier","icon-magnifier-add","icon-magnifier-remove","icon-paper-clip","icon-paper-plane","icon-power","icon-refresh","icon-reload","icon-settings","icon-star","icon-symbol-female","icon-symbol-male","icon-target","icon-credit-card","icon-paypal","icon-social-tumblr","icon-social-twitter","icon-social-facebook","icon-social-instagram","icon-social-linkedin","icon-social-pinterest","icon-social-github","icon-social-google","icon-social-reddit","icon-social-skype","icon-social-dribbble","icon-social-behance","icon-social-foursqare","icon-social-soundcloud","icon-social-spotify","icon-social-stumbleupon","icon-social-youtube","icon-social-dropbox","icon-social-vkontakte","icon-social-steam");
		$this->current_menu = "section";
	}
	
	function edit($id){
		if (is_post()) {
			$item = $this->input->post();
			$this->db->where('id', $id);
			$this->db->update('section_services', $item); 

			$this->session->set_flashdata('message','Data Saved Successfully');
			redirect(base_url("manage/section_services/edit/$id"));
		}

		$data['id'] 		= $id;
		$data['master'] 	= $this->db->get_where('section_services',array('id'=>$id))->row();
		$data['items'] 		= $this->db->get_where('section_services_item',array('services_id'=>$id));
		$data['iconset'] 	= $this->iconset;
		$data['local_view'] = 'v_section_services';
		$this->load->view('v_manage',$data);
	}

	function upload(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'assets/section';
		$config['allowed_types']	= 'jpg|png';
		$config['max_size']			= '10000';
		$config['max_width']		= '5000';
		$config['max_height']		= '5000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			return array(true,$a);
		}
	}

	function add_item(){
		$data = $this->input->post();
		$d = $this->upload();
		$data['image'] 	= $d[1]['file_name'];
		$this->db->insert('section_services_item',$data);
		
		$this->session->set_flashdata('message','Data Saved Successfully');
		redirect(base_url("manage/section_services/edit/".$data['services_id']));
	}

	function delete_item($id,$ids){
		$this->db->delete('section_services_item', array('id' => $id)); 
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(base_url("manage/section_services/edit/$ids"));
	}

}