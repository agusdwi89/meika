<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section_slider extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "section";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	function edit($id){
		$data['id'] 		= $id;
		$data['items'] 		= $this->db->get_where('section_slider_items',array('slider_id'=>$id));
		$data['section']   	= $this->db->get_where('v_section_name',array('subdomain'=>$this->sub_domain));
		$data['local_view'] = 'v_section_slider';
		$this->load->view('v_manage',$data);
	}

	function upload(){
		$dataInfo = array();
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);
		for($i=0; $i<$cpt; $i++)
		{           
			$_FILES['userfile']['name']		= strtolower($files['userfile']['name'][$i]);
			$_FILES['userfile']['type']		= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']	= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']		= $files['userfile']['size'][$i];    

			$config['upload_path']		= 'assets/section';
			$config['allowed_types']	= 'jpg|png';
			$config['max_size']			= '10000';
			$config['max_width']		= '5000';
			$config['max_height']		= '5000';
			$config['encrypt_name']		= true;
			$this->load->library('upload', $config);
			$this->upload->do_upload();
			$dataInfo[] = $this->upload->data();
		}
		return $dataInfo;
	}

	function add_item(){
		$d = $this->upload();
		$data 			= $this->input->post('def');
		$data['image'] 	= $d[0]['file_name'];
		$data['url'] 	= $this->input->post($data['link']);

		$this->db->insert('section_slider_items',$data);
		$id = $data['slider_id'];
		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url("manage/section_slider/edit/$id"));
	}

	function delete_item($id,$ids){
		$this->db->delete('section_slider_items', array('id' => $id)); 
		$this->session->set_flashdata('message', 'Delete successfully');
		redirect(base_url("manage/section_slider/edit/$ids"));
	}
}