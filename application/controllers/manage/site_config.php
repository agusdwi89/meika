<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_config extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "config";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function index()
	{
		if (is_post()) {
			$data = $this->input->post();
			foreach ($data as $key => $value) {
				$data = array('value' => $value );
				$this->db->where('name', $key);
				$this->db->where('subdomain', $this->sub_domain);
				$this->db->update('site_config', $data);
			}
			$this->session->set_flashdata('tab','global');	
			$this->session->set_flashdata('message','Data Saved Successfully');	
			redirect(base_url('manage/site_config'));
		}

		if ($this->sub_domain != 'all') {
			$data['data'] = array(
				"site-title"		=> $this->db->get_where('site_config',array('name'=>'site-title', 'subdomain' => "$this->sub_domain"))->row()->value,
				"top-title"			=> $this->db->get_where('site_config',array('name'=>'top-title', 'subdomain' => "$this->sub_domain"))->row()->value,
				"top-subtitle"		=> $this->db->get_where('site_config',array('name'=>'top-subtitle', 'subdomain' => "$this->sub_domain"))->row()->value,
				"meta-description"	=> $this->db->get_where('site_config',array('name'=>'meta-description', 'subdomain' => "$this->sub_domain"))->row()->value,
				"meta-keywords"		=> $this->db->get_where('site_config',array('name'=>'meta-keywords', 'subdomain' => "$this->sub_domain"))->row()->value,
				// "email-footer"		=> $this->db->get_where('site_config',array('name'=>'email-footer', 'subdomain' => "$this->sub_domain"))->row()->value,
				// "user-buy"			=> $this->db->get_where('site_config',array('name'=>'user-buy', 'subdomain' => "$this->sub_domain"))->row()->value,
				// "user-see-bottom"	=> $this->db->get_where('site_config',array('name'=>'user-see-bottom', 'subdomain' => "$this->sub_domain"))->row()->value,
				// "user-see-top"		=> $this->db->get_where('site_config',array('name'=>'user-see-top', 'subdomain' => "$this->sub_domain"))->row()->value,
				// "google-track"		=> $this->db->get_where('site_config',array('name'=>'google-track', 'subdomain' => "$this->sub_domain"))->row()->value,
				// "fb-track"			=> $this->db->get_where('site_config',array('name'=>'fb-track', 'subdomain' => "$this->sub_domain"))->row()->value,
			);
		} 

		$data['local_view'] 	= 'v_site_config';
		$data['value_pure'] 	= $this->db->get_where('site_config',array('id'=>1001))->row()->value;
		$this->load->view('v_manage',$data);
	}

	function set_logo(){
		$this->upload('logo.png');
		$this->session->set_flashdata('message','Data Saved Successfully');	
		$this->session->set_flashdata('tab','logo');	
		redirect(base_url('manage/site_config'));
	}

	function set_logo1(){
		$this->upload('logo_1.png');
		$this->session->set_flashdata('message','Data Saved Successfully');	
		$this->session->set_flashdata('tab','logo');	
		redirect(base_url('manage/site_config'));
	}

	function set_logo2(){
		$this->upload('logo-black.png');
		$this->session->set_flashdata('message','Data Saved Successfully');	
		$this->session->set_flashdata('tab','logo');	
		redirect(base_url('manage/site_config'));
	}


	function set_pure(){
		$item = $this->input->post();

		$this->db->where('id', $item['id']);
		$this->db->update('site_config', array('value'=>$item['value'])); 
		
		$this->session->set_flashdata('message','Data Saved Successfully');	
		$this->session->set_flashdata('tab','pure');	
		redirect(base_url('manage/site_config'));
	}

	function upload($name){
		$_FILES['userfile']['name']		= strtolower($_FILES['userfile']['name']);
		$config['upload_path']			= 'assets/qs/images/logo/';
		$config['allowed_types']		= '*';
		$config['max_size']				= '10000';
		$config['max_width']			= '800';
		$config['max_height']			= '1024';
		$config['overwrite'] 			= TRUE;
		$config['file_name'] 			= $name;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			return array(true,$a);
		}
	}

}