<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Theme extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "theme";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function index()
	{
		if (is_post()) {
			$data = array('value' => $this->input->post('theme'));
			$this->db->where('subdomain', $this->sub_domain);
			$this->db->where('name', 'theme');
			$this->db->update('site_config', $data); 

			if ($this->input->post('theme') == "custom") {
				$this->generate_custom_theme();
			}

			$this->session->set_flashdata('message','theme config saved successfully');
			redirect(base_url('manage/theme'));
		}

		$theme_config 			= $this->db->get_where('site_config',array('subdomain'=>$this->sub_domain,'name'=>'theme'))->row();
		$data['local_view'] 	= 'v_theme';
		$data['theme_choosen'] 	= $theme_config->value;
		$data['custom_color'] 	= json_decode($theme_config->value2);

		$this->load->view('v_manage',$data);
	}

	function generate_custom_theme(){
		$cst 	= $this->input->post('custom');
		$json 	= json_encode($cst);

		// readfile & write file
		$this->load->helper('file');
		$string = read_file('assets/qs/css/colors/def.css');

		foreach ($cst as $key => $value) {
			$string = str_replace("{".$key."}", $value, $string);
		}

		$string = str_replace("{grad1}", $this->hexToRgb($cst['primary-color']), $string);
		$string = str_replace("{grad2}", $this->hexToRgb($cst['secondary-color']), $string);

		write_file('assets/qs/css/colors/custom.css', $string);

		//update db
		$data = array('value2' => $json);
		$this->db->where('subdomain', $this->sub_domain);
		$this->db->where('name', 'theme');
		$this->db->update('site_config', $data);
	}

	function readdefcss(){
		$this->load->helper('file');
		$string = read_file('assets/fe/css/colors/def.css');
		$string = str_replace("defcolor1", "agusdwip", $string);
		debug_array($string);
	}

	function hexToRgb($hex) {
		$hex      = str_replace('#', '', $hex);
		$length   = strlen($hex);
		$rgb['r'] = hexdec($length == 6 ? substr($hex, 0, 2) : ($length == 3 ? str_repeat(substr($hex, 0, 1), 2) : 0));
		$rgb['g'] = hexdec($length == 6 ? substr($hex, 2, 2) : ($length == 3 ? str_repeat(substr($hex, 1, 1), 2) : 0));
		$rgb['b'] = hexdec($length == 6 ? substr($hex, 4, 2) : ($length == 3 ? str_repeat(substr($hex, 2, 1), 2) : 0));
		return implode(",", $rgb);
	}
}