<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	function __construct(){
		parent::__construct();
		if ($this->db->get_where('_subdomain',array('subdomain'=>subdomain()))->num_rows() == 0) {
			show_404();
		 }
	}

	function index(){
		$data['list_section']	= $this->get_footer();
		$data['news'] 			= $this->db->order_by('date','desc')->get('news');
		$this->load->view('v_news',$data);
	}

	function read($slug){
		$item = $this->db->limit(1)->get_where('news',array('slug'=>$slug));
		if($item->num_rows() == 0) show_404();
		
		$data['item'] 			= $item->row();
		$this->load->view('v_news_detail',$data);	
	}
}