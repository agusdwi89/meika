<style type="text/css">.thanks{background-position: 50% 0px;position: fixed;top: 0;bottom: 0;right: 0;left: 0;}#btn-cek{color: black;}input{color: black;}</style>
<script language="JavaScript" type="text/javascript" src="https://staging.doku.com/dateformat.js"></script>
<script language="JavaScript" type="text/javascript" src="https://staging.doku.com/sha-1.js"></script>
<script type="text/javascript">
function getRequestDateTime() {
	var now = new Date();
	document.MerchatPaymentPage.REQUESTDATETIME.value = dateFormat(now, "yyyymmddHHMMss");	
}

function randomString(STRlen) {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = STRlen;
	var randomstring = '';
	for (var i=0; i<string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
	return randomstring;
}

function genInvoice() {	
	<?if ($data->payment_detail == ""): ?>
		document.MerchatPaymentPage.TRANSIDMERCHANT.value = randomString(12);
	<?else:?>
		document.MerchatPaymentPage.TRANSIDMERCHANT.value = "<?=$data->payment_detail;?>";
	<?endif;?>
}

function genSessionID() {	
	document.MerchatPaymentPage.SESSIONID.value = randomString(20);
}

function genBookingCode() {	
	document.MerchatPaymentPage.BOOKINGCODE.value = randomString(6);
}

function getWords() {
	var msg = document.MerchatPaymentPage.AMOUNT.value + document.MerchatPaymentPage.MALLID.value + document.MerchatPaymentPage.SHAREDKEY.value + document.MerchatPaymentPage.TRANSIDMERCHANT.value;
	document.MerchatPaymentPage.WORDS.value = SHA1(msg);	
}
</script>

<section id="technologies" class="technologies block thanks" style="background-position: 50% -20px;background: #333e50;">
	<div class="overlay-tech"></div>
	<div class="container tech-container"><br><br>
		<center><b>Tunggu sebentar, anda akan di arahkan ke halaman pembayaran doku</b></center>
		<form style="display:none;" action="<?=$this->doku_url;?>" id="MerchatPaymentPage" name="MerchatPaymentPage" method="post" >
			BASKET <input name="BASKET" type="text" id="BASKET" value="<?=$data->prd_title;?>,<?=$data->prd_total;?>,1,<?=$data->prd_total;?>" size="100" />
			MALLID <input name="MALLID" type="text" id="MALLID" value="<?=$this->mall_id;?>" size="12" />
			CHAINMERCHANT <input name="CHAINMERCHANT" type="text" id="CHAINMERCHANT" value="NA" size="12" />
			CURRENCY <input name="CURRENCY" type="text" id="CURRENCY" value="360" size="3" maxlength="3" />
			PURCHASECURRENCY <input name="PURCHASECURRENCY" type="text" id="PURCHASECURRENCY" value="360" size="3" maxlength="3" />
			AMOUNT <input name="AMOUNT" type="text" id="AMOUNT" value="<?=$data->prd_total;?>.00" size="12" />
			PURCHASEAMOUNT <input name="PURCHASEAMOUNT" type="text" id="PURCHASEAMOUNT" value="<?=$data->prd_total;?>.00" size="12" />
			TRANSIDMERCHANT <input name="TRANSIDMERCHANT" type="text" id="TRANSIDMERCHANT" size="16" />
			SHAREDKEY <input name="SHAREDKEY" type="text" id="SHAREDKEY" value="<?=$this->shared_key;?>" size="15" maxlength="12"/>
			WORDS	<input type="text" id="WORDS" name="WORDS"  size="60" />&nbsp;&nbsp;<input type="button" value="Generate WORDS" onClick="getWords();">&nbsp;
			REQUESTDATETIME	<input name="REQUESTDATETIME" type="text" id="REQUESTDATETIME" size="14" maxlength="14" />
			SESSIONID	<input type="text" id="SESSIONID" name="SESSIONID" />
			PAYMENTCHANNEL	<input type="text" id="PAYMENTCHANNEL" name="PAYMENTCHANNEL" value="<?=$this->payment_channel;?>" />
			EMAIL	<input name="EMAIL" type="text" id="EMAIL" value="<?=$data->cust_email;?>" size="12" />
			NAME	<input name="NAME" type="text" id="NAME" value="<?=$data->cust_f_name;?>" size="30" maxlength="50" />
			ADDRESS	<input name="ADDRESS" type="text" id="ADDRESS" value="<?=$data->cust_address;?>, <?=$data->cust_kecamatan;?>" size="50" maxlength="50" />
			COUNTRY	<input name="COUNTRY" type="text" id="COUNTRY" value="360" size="50" maxlength="50" />
			STATE	<input name="STATE" type="text" id="STATE" value="Jakarta" size="50" maxlength="50" />
			CITY	<input name="CITY" type="text" id="CITY" value="JAKARTA TIMUR" size="50" maxlength="50" />
			PROVINCE	<input name="PROVINCE" type="text" id="PROVINCE" value="JAKARTA" size="50" maxlength="50" />
			ZIPCODE	<input name="ZIPCODE" type="text" id="ZIPCODE" value="<?=$data->cust_zip;?>" size="6" maxlength="10" />
			HOMEPHONE	<input name="HOMEPHONE" type="text" id="HOMEPHONE" value="<?=$data->cust_phone;?>" size="12" maxlength="20" />
			MOBILEPHONE	<input name="MOBILEPHONE" type="text" id="MOBILEPHONE" value="<?=$data->cust_phone;?>" size="12" maxlength="20" />
			WORKPHONE	<input name="WORKPHONE" type="text" id="WORKPHONE" value="<?=$data->cust_phone;?>" size="12" maxlength="20" />
			BIRTHDATE	<input name="BIRTHDATE" type="text" id="BIRTHDATE" value="19710101" size="12" maxlength="8" />
			<input name="submit" type="submit" class="bt_submit" id="submit" value="SUBMIT" />
		</form>
	</div>
</section>

<script language="javascript" type="text/javascript">

genInvoice();
getWords();
getRequestDateTime();
genSessionID();

$(function(){
	var transidmerchant = $("#TRANSIDMERCHANT").val();
	$.post("<?=base_url('doku/request_pay')?>", {'transidmerchant':transidmerchant,'id':<?=$data->id;?>},function(data){
		if (data.message == "success") {
			setTimeout(function () {
				$("#submit").click();
			}, 2000);
		};
	},'json');
})
</script>