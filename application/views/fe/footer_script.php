<!-- Include JS -->
<script src="<?=base_url()?>assets/fe/js/jquery.min.js"></script>
<script src="<?=base_url()?>assets/fe/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/fe/js/device.min.js"></script>
<script src="<?=base_url()?>assets/fe/js/retina.min.js"></script>
<script src="<?=base_url()?>assets/fe/js/nav.js"></script>
<script src="<?=base_url()?>assets/fe/js/smooth-scroll.min.js"></script>
<script src="<?=base_url()?>assets/fe/js/parallax.js"></script>
<script src="<?=base_url()?>assets/fe/js/aos.js"></script>
<script src="<?=base_url()?>assets/fe/js/flexslider.js"></script>
<script src="<?=base_url()?>assets/fe/js/youtubebackground.js"></script>
<script src="<?=base_url()?>assets/fe/js/magnific-popup.min.js"></script>
<script src="<?=base_url()?>assets/fe/js/init.js"></script>

<!--[if lte IE 9]>
<script src="<?=base_url()?>assets/fe/js/placeholders.js"></script>
<script src="<?=base_url()?>assets/fe/js/init-for-ie.js"></script>
<![endif]-->

<!-- plugin -->
<script src="<?=base_url()?>assets/plugins/flipclock/flipclock.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/flipclock/flipclock.css">

<script type="text/javascript">
	var clock;
	
	$(document).ready(function() {
		clock = $('.clock').FlipClock(35000, {
			countdown: true,
			autoStart: true
		});

	});
</script>