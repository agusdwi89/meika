<div id="content-confirmation">
	<style type="text/css">
		#order-step2 h4{
			margin:0.5rem 0;
		}
		#order-step2 table tr td{
			padding: 5px;
		}
		#order-step2 p{
			text-align: left;
			margin-bottom: 0.25rem;
		}
		#row-total td{
			background: #face0b2e;
			border-top: 2px solid #8a8383;
			font-weight: bold;
			font-size: 16px;
		}
	</style>
	<script type="text/javascript">
		var order_status = 'incompleted';
		$(function(){
			var ths;
			$('body').on('click','#order-button-final',function (e) {
				e.preventDefault();
				$("#order-step2").fadeOut(function(){
					ths = $(this);
					ths.before("<br><br><p id='confirm_loading'>Please wait . . .</p>");
					$.post("<?=base_url('home/place_order')?>", $("#order-form").serialize(),function(data){
						order_status = 'completed';
						$("#confirm_loading").hide();
						if (data.status == "failed") {
							ths.before("<br><br><p>Order gagal, silahkan coba lagi");
						}else{
							ths.before("<br><br><p>Terimakasih sudah order!<br> Tunggu sebentar anda akan diarahkan ke halaman order");
							var payment = data.payment;var url="";
							
							if (payment.search("doku") >= 0) {
								url = "<?=base_url()?>home/order/"+data.link_unique;
							}else{
								url = "<?=base_url()?>order_success/"+data.link_unique;
							}
							
							setTimeout(function () {
								window.location.href = url;
    						}, 2000);
						}

					},'json');
				})
			});
		})

		var price_final 	= 0;
		var grand_total 	= 0;
		var coupon_final	= 0;
		var delivery_final 	= 0;
		var coupon_code_final = "";
		var confirmKurir = "";
		var confirmPay = "";
		
		function setConfirmation(){
			price_final = ((real_prc_dsc > 0) ? real_prc_dsc : real_prc);
			price_final = price_final * qtyval;
			confirmKurir = $("#order-courier option:selected");
			delivery_final = confirmKurir.data('kurir-price');
			
			$("#confirm-ship").html("Rp. "+formatNumber(delivery_final));
			$("#confirm-ship-type").html(confirmKurir.data('kurir'));
			var qtytext = ((qtyval > 1)? " ("+qtyval+"X)" : "");
			$("#confirm-prd").text($("#pop-title").html()+qtytext);
			$("#confirm-price").text("Rp. "+formatNumber(price_final));

			if (coupon_final > 0) {
				$("#row-coupon").removeClass("hide");
				$("#confirm-coupon-code b").text($("#hd-prd_coupon_code").val());
				$("#confirm-coupon-code-value").text("Rp. "+formatNumber(coupon_final));
			}else{
				$("#row-coupon").addClass("hide");
			}

			grand_total = price_final + delivery_final - coupon_final;

			$("#confirm-grand-total").text("Rp. "+formatNumber(grand_total));

			$("#hd-prd_total").val(grand_total);

			setDeliveryDetail();
		}

		function setDeliveryDetail(){
			$("#deliver-name").html($("#order-f-name").val());
			$("#deliver-address").html($("#order-address").val());
			$("#deliver-prov").html($("#order-province option:selected").text()+" - "+$("#order-city option:selected").text()+"<br>Kec. "+$("#order-kecamatan option:selected").text()+", "+$("#order-zip").val());
			$("#deliver-email").html("Email : "+$("#order-email").val()+", HP : "+$("#order-phone").val());
			$("#deliver-shipping").html("Shipping Method : "+(confirmKurir.data("kurir")).toUpperCase());
			$("#deliver-payment").html("Payment : "+confirmPay);
		}

		function set_close_modal(){
			if (order_status == 'completed') {
				location.reload();
			}
		}

	</script>
	<div id="order-step2">
		<br>
		<div class="col-md-12"><h4>Ringkasan Pemesanan</h4></div>
		<div class="col-md-12">
			<table class="table table-striped">
				<tr>
					<td id="confirm-prd"></td>
					<td style="text-align:right" id="confirm-price">Rp 2.000.000</td>
					<td></td>
				</tr>
				<tr>
					<td>Jasa Pengiriman : <span style="text-transform: uppercase;font-weight: bold;" id="confirm-ship-type"></span></td>
					<td id="confirm-ship" style="text-align:right"></td>
					<td></td>
				</tr>
				<tr id="row-coupon" class="hide">
					<td id="confirm-coupon-code">Coupon Code : <b></b></td>
					<td id="confirm-coupon-code-value" style="text-align:right"></td>
					<td>(-)</td>
				</tr>
				<tr id="row-total">
					<td>Total</td>
					<td id="confirm-grand-total" style="text-align:right"></td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="col-md-12"><h4>Dikirim Ke</h4></div>
		<div class="col-md-12">
			<b id="deliver-name">Agus, Prayogo</b>
			<p id="deliver-address">Jalan kjlhd kalsjdsad asjdasd,</p>
			<p id="deliver-prov">ZipCode : 29832, Prov. Sulut, Kab. alsjdasd</p>
			<P id="deliver-email">Email : 23948324, HP : 238493204</P>
			<P id="deliver-shipping">Shipping Method : JNE</P>
			<P id="deliver-payment">Payment Method : Transfer Bank</P>
		</div>
		<div class="col-md-12 order-button-block">
			<button id="order-button-final" type="submit" class="button order-button" title="Order">Konfirmasi Pesanan</button>
		</div>
	</div>
</div>