<?$dbtpm = $this->db->get_where('top_menu',array('subdomain'=>subdomain()));?>
<!--=========== Start Header Nav ===============-->
<div id="header" class="menubar">
	<div class="menubar-content">
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-2">
						<div class="site-title">
							<a class="navbar-brand call-section" href="<?=base_url()?>"><img class="nav-logo" src="<?=base_url()?>assets/qs/images/logo/logo_1.png" alt="logo"></a>
						</div><!-- end site-title -->
					</div><!-- end col-md-4 -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button><!-- end button -->
					</div><!-- end navbar-header -->
					<div class="col-md-10 col-sm-10 navbar-style">
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<?php $i=0; foreach ($dbtpm->result() as $v): $i++;?>
									<li class="<?=($i==1) ? 'current' : '';?>">
										<?if ($v->link == "internal"): ?>
											<a href="#<?=$v->url;?>" data-scroll="<?=$v->url;?>"><?=$v->name;?></a>
										<?else:?>
											<a href="<?=$v->url;?>" target="_blank"><?=$v->name;?></a>
										<?endif;?>
									</li>
								<?php endforeach ?>
							</ul>
						</div>
					</div>
					<!-- end col-md-8 -->
				</div><!-- end row -->
			</div><!-- end container-fluid -->
		</nav><!-- navbar -->
	</div><!-- end menubar-content -->
</div><!-- end menubar -->