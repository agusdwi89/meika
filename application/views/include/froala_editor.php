<link rel="stylesheet" href="<?=base_url()?>assets/plugins/froala/css/froala_editor.css">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/froala/css/froala_style.css">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/froala/css/plugins/code_view.css">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/froala/css/plugins/image_manager.css">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/froala/css/plugins/image.css">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/froala/css/plugins/table.css">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/froala/css/plugins/video.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/froala_editor.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/align.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/code_view.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/draggable.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/image.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/image_manager.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/link.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/lists.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/paragraph_format.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/table.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/video.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/url.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/froala/js/plugins/entities.min.js"></script>

<style type="text/css">
    .fr-wrapper div:first-child a{
        display: none !important;
    }
</style>

<script type="text/javascript">
$(function(){
	var paramz = new Object;
	paramz[$("#global-form input").attr('name')] = $("#global-form input").val();

	$('.selector').froalaEditor({
            height: 200,
    		// Set the image upload parameter.
    		imageUploadParam: 'userfile',

    		// Set the image upload URL.
    		imageUploadURL: '<?=base_url()?>manage/news/upload',

    		// Additional upload params.
    		imageUploadParams: paramz,

    		// Set request type.
    		imageUploadMethod: 'POST',

    		// Set max image size to 5MB.
    		imageMaxSize: 5 * 1024 * 1024,

    		// Allow to upload PNG and JPG.
    		imageAllowedTypes: ['jpeg', 'jpg', 'png']
    	}).on('froalaEditor.image.beforeUpload', function (e, editor, images) {
    		// Return false if you want to stop the image upload.
    	}).on('froalaEditor.image.uploaded', function (e, editor, response) {
    		// Image was uploaded to the server.
    	}).on('froalaEditor.image.inserted', function (e, editor, $img, response) {
    		// Image was inserted in the editor.
    	}).on('froalaEditor.image.replaced', function (e, editor, $img, response) {
    		// Image was replaced in the editor.
    	}).on('froalaEditor.image.error', function (e, editor, error, response) {
    		// Bad link.
    		if (error.code == 1) {}
				// No link in upload response.
			else if (error.code == 2) {}
				// Error during image upload.
			else if (error.code == 3) {}
				// Parsing response failed.
			else if (error.code == 4) {}
    			// Image too text-large.
    		else if (error.code == 5) {}
				// Invalid image type.
			else if (error.code == 6) {}
				// Image can be uploaded only to same domain in IE 8 and IE 9.
			else if (error.code == 7) {}
				// Response contains the original server response to the request if available.
		});
    })
</script>