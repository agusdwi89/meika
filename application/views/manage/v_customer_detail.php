<style type="text/css">
	#datatable_wrapper{opacity: 0}
	#datatable{opacity: 0}
	p.txwrap{
		max-width:200px !important;
		overflow-wrap: break-word !important;
		word-wrap: break-word !important;
		white-space: pre-wrap;
		line-height: 15px;
		font-size: 11px;
		font-style: italic;
	}
	a.btn-confirm-download span{
		font-size: 10px;
		display: block;
		float: right;
		margin-left: 5px;
	}
	a.btn-confirm-download i{
		margin-top: 9px;
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#datatable').dataTable({
			"initComplete": function(settings, json) {
				$("#loader").remove();
				$("#datatable_wrapper").css('opacity', '1');
				$("#datatable").css('opacity', '1');
			}
		});
	})
</script>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Customer Detail</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">
						<h4 class="m-t-0 header-title">
							<b>List Order :: <?=$cust->cust_email;?></b>
							<a id="btn-save-order" href="<?=base_url('manage/customer')?>" class="badge badge-info">customer list</a>
						</h4>
						<br>
						<div id='loader'><center>Loading data . . . <br></center></div>

						<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
							<thead>
								<tr>
									<th>No</th>
									<th>Customer Identity</th>
									<th>Product</th>
									<th>Price</th>
									<th class="text-center">Order Detail</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?$i=0;foreach ($db->result() as $d): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td>
											<b><?=$d->cust_f_name;?></b><br>
											e : <?=$d->cust_email;?><br>
											h : <?=$d->cust_phone;?><hr>
											<p class="txwrap"><?=$d->cust_address;?></p>
											<?=$d->province;?><br><?=$d->city;?><br><?=$d->cust_zip?>
										</td>
										<td>
											<img width="50" src="<?=base_url()?>assets/section/<?=$d->prd_image;?>">
											<div>
												<b><?=$d->prd_title;?></b>
												<p style="margin-bottom: 0.1rem"><?=$d->prd_description;?></p>
												<p style="margin-bottom: 0.1rem">Rp. <?=format_number($d->prd_price);?></p>
												<span class="badge badge-purple">subdomain : <?=$map_product_domain[$d->prd_id]['sd']->subdomain?></span>
											</div>
										</td>
										<td>
											<h4>Rp. <?=format_number($d->prd_total);?></h4>
											<b>Qty : <?=$d->order_quantity?> pcs</b><br>
											<b>item: Rp. <?=format_number($d->prd_price * $d->order_quantity)?></b><br>
											<b>shipping: Rp. <?=format_number($d->prd_shipment);?></b><br>
											<?if ($d->prd_coupon > 0): ?>
												<b>coupon (-): Rp. <?=format_number($d->prd_coupon)?></b><br>
											<?endif;?>
										</td>
										<td class="text-center">
											<?=format_date_time($d->time_order);?><br>
											order ID : <b><?="#SB1".sprintf('%05d', $d->id);?></b><br>
											<br>status :	<br>
											<?if ($d->order_status == "wait-payment"): ?>
												<span class="badge badge-danger">menunggu-pembayaran</span>
											<?elseif($d->order_status == "payment-success"):?>
												<span class="badge badge-info">pembayaran-sukses</span>
											<?elseif($d->order_status == "delivery-process"):?>
												<span class="badge badge-info">proses-pengiriman</span>
											<?else: ?>
												<span class="badge badge-success">pesanan-diterima</span>
											<?endif;?>
											<br>
											<br>payment method :	<br>
											<span class="badge badge-purple"><?=$d->payment;?></span>
										</td>
										<td>
											<br>
											<div class="btn-group m-b-10" style="display: block;margin: 0 auto;text-align: center;">
												<a target="_blank" href="<?=base_url()?>manage/order/detail/<?=$d->id;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-purple dz-tip" title="Detail Info"> <i class="fa fa-arrow-circle-right"></i> </a>
												<a target="_blank" href="<?=base_url()?>home/order/<?=$d->link_unique;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-purple dz-tip" title="Show on Customer Page"> <i class="fa fa-search"></i> </a>
												<?php if ($d->receipt != ""): ?>
													<br><br>
													<a href="<?=base_url()?>assets/receipt/<?=$d->receipt;?>"
														title="show customer payment confirmation"
														class="btn-confirm-download btn btn-success waves-effect waves-light btn-sm dz-tip" target="_blank"> 
														<i class="fa fa-file-image-o m-r-5"></i> 
														<span>payment<br>confirmation</span> 
													</a>													
												<?php endif ?>
												
											</div>
										</td>
									</tr>
								<?endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>