<style type="text/css">
	#datatable_wrapper{opacity: 0}
	#datatable{opacity: 0}
</style>
<script type="text/javascript">
	$(function(){
		$('#datatable').dataTable({
			"initComplete": function(settings, json) {
				$("#loader").remove();
				$("#datatable_wrapper").css('opacity', '1');
				$("#datatable").css('opacity', '1');
			}
		});
	})
</script>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage User Question</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">
						<h4 class="m-t-0 header-title">
							<b>List of user question</b>
							<a href="<?=base_url('manage/news/add')?>" style="float: right" class="btn btn-info waves-effect waves-light btn-sm"> <i class="fa fa-plus m-r-5"></i> <span>Add News</span> </a>
						</h4>
						<br>
						<div id='loader'><center>Loading data . . . <br></center></div>

						<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
							<thead>
								<tr>
									<th>No</th>
									<th>Name</th>
									<th>Email</th>
									<th>Subject</th>
									<th>Message</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								<?$i=0;foreach ($db->result() as $v): $i++;?>
								<tr>
									<td><?=$i;?></td>
									<td><?=$v->name;?></td>
									<td><?=$v->email;?></td>
									<td><?=$v->subject;?></td>
									<td><?=$v->message;?></td>
									<td><?=format_date_time($v->date_created,false);?></td>
								</tr>
								<?endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>