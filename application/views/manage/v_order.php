<style type="text/css">
	#datatable_wrapper{opacity: 0}
	#datatable{opacity: 0}
	p.txwrap{
		max-width:200px !important;
		overflow-wrap: break-word !important;
		word-wrap: break-word !important;
		white-space: pre-wrap;
		line-height: 15px;
		font-size: 11px;
		font-style: italic;
	}
	a.btn-confirm-download span{
		font-size: 10px;
		display: block;
		float: right;
		margin-left: 5px;
	}
	a.btn-confirm-download i{
		margin-top: 9px;
	}
</style>
<script type="text/javascript">
	var start 		= moment().startOf('month');
	var end 		= moment().endOf('month');
	var url_batch 	= "<?=base_url('manage/order/batch')?>";
	var ad = start.format('YYYY-MM-DD');var bd = end.format('YYYY-MM-DD');

	$(function(){
		function cb(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}

		$('#reportrange').daterangepicker({
			startDate: moment().startOf('month'),
			endDate: moment().endOf('month'),
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);

		cb(start, end);

		$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			ad = picker.startDate.format('YYYY-MM-DD');
			bd = picker.endDate.format('YYYY-MM-DD');
			load_order(ad,bd);
		});
		load_order(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));

		// batch
		$('.bs-example-modal-lg').on('show.bs.modal', function() { 
			$("#batch-modal-body").html('');
			var opt = {s:ad,e:bd};
			opt[$("#global-form input").attr('name')] = $("#global-form input").val();
			$.post( url_batch,opt, function( data ) {
				$("#batch-modal-body").html(data);
			});
		}) ;
	})

	function load_order(s,e){
		var url = "<?=base_url()?>manage/order/load/"+s+"s"+e;
		$("#main-load").html("");
		$("#loader-bar").show();
		$.get( url, function( data ) {
			$("#main-load").html(data);
		});
	}
</script>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Order</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">
						<div class="row">
							<div class="col-md-12">
								<h4 style="line-height:30px" class="m-t-0 header-title pull-left">List Customer Order [ subdomain : <?=(($this->sub_domain == "all")? "All sub-domain" : $this->sub_domain.".".$_SERVER['HTTP_HOST']);?> ]</h4>
								<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;" class="pull-right">
									<i class="fa fa-calendar"></i>&nbsp;
									<span></span> <i class="fa fa-caret-down"></i>
								</div>	
								<a title="download csv" href="<?=base_url('manage/order/download')?>" style="float: right;margin-right:5px" class="btn btn-info waves-effect waves-light btn-sm dz-tip"><i class="fa fa-cloud-download"></i></a>
								<a title="batch update status order" href="#" style="float: right;margin-right:5px" class="btn btn-warning waves-effect waves-light btn-sm dz-tip" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-pencil"></i></a>
							</div>
						</div>
						<br>
						<div id="main-load"></div>
						<div 
							id="loader-bar"
							style="width: 50%;margin: 0 auto;height: 15px;border-radius: 10px;margin-top: 20px;max-width: 250px;" 
							class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"
							>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt-0" id="myLargeModalLabel">Batch Update Status Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="batch-modal-body" class="modal-body">
				...
			</div>
		</div>
	</div>
</div>