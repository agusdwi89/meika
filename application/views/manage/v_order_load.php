<script type="text/javascript">
	$(function(){
		$('#datatable').dataTable({
			"initComplete": function(settings, json) {
				$("#loader-bar").hide();
				$("#datatable_wrapper").css('opacity', '1');
				$("#datatable").css('opacity', '1');
			}
		});
	})
</script>
<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
	<thead>
		<tr>
			<th>No</th>
			<th>Customer Identity</th>
			<th>Product</th>
			<th>Price</th>
			<th class="text-center">Order Detail</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?$i=0;foreach ($db as $d): $i++;?>
			<tr>
				<td><?=$i;?></td>
				<td>
					<b><?=$d->cust_f_name;?></b><br>
					e : <?=$d->cust_email;?><br>
					h : <?=$d->cust_phone;?><hr>
					<p class="txwrap"><?=$d->cust_address;?></p>
					<?=$d->province;?><br><?=$d->city;?><br><?=$d->cust_zip?>
				</td>
				<td>
					<img width="50" src="<?=base_url()?>assets/section/<?=$d->prd_image;?>">
					<div>
						<b><?=$d->prd_title;?></b>
						<p style="margin-bottom: 0.1rem"><?=$d->prd_description;?></p>
						<p style="margin-bottom: 0.1rem">Rp. <?=format_number($d->prd_price);?></p>
						<span class="badge badge-purple">subdomain : <?=$d->subdomain;?></span>
					</div>
				</td>
				<td>
					<h4>Rp. <?=format_number($d->prd_total);?></h4>
					<b>Qty : <?=$d->order_quantity?> pcs</b><br>
					<b>item: Rp. <?=format_number($d->prd_price * $d->order_quantity)?></b><br>
					<b>shipping: Rp. <?=format_number($d->prd_shipment);?></b><br>
					<?if ($d->prd_coupon > 0): ?>
						<b>coupon (-): Rp. <?=format_number($d->prd_coupon)?></b><br>
					<?endif;?>
				</td>
				<td class="text-center">
					<?=format_date_time($d->time_order);?><br>
					order ID : <b><?="#SB1".sprintf('%05d', $d->id);?></b><br>
					<br>status :	<br>
					<?if ($d->order_status == "wait-payment"): ?>
						<?php if ($d->payment == "cod"): ?>
							<span class="badge badge-danger">menunggu-konfirmasi</span>    
						<?php else: ?>
							<span class="badge badge-danger">menunggu-pembayaran</span>
						<?php endif ?>
					<?elseif($d->order_status == "payment-success"):?>
						<?php if ($d->payment == "cod"): ?>
							<span class="badge badge-info">konfirmasi-pesanan</span>    
						<?php else: ?>
							<span class="badge badge-info">konfirmasi-pesanan</span>
						<?php endif ?>
					<?elseif($d->order_status == "delivery-process"):?>
						<span class="badge badge-info">proses-pengiriman</span>
					<?else: ?>
						<?php if ($d->payment == "cod"): ?>
							<span class="badge badge-success">pembayaran-diterima & pengiriman-selesai</span>    
						<?php else: ?>
							<span class="badge badge-success">pesanan-diterima</span>
						<?php endif ?>
					<?endif;?>
					<br>
					<br>payment method :	<br>
					<span class="badge badge-purple"><?=$d->payment;?></span>
				</td>
				<td>
					<br>
					<div class="btn-group m-b-10" style="display: block;margin: 0 auto;text-align: center;">
						<a href="<?=base_url()?>manage/order/detail/<?=$d->id;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-purple dz-tip" title="Detail Info"> <i class="fa fa-arrow-circle-right"></i> </a>
						<a target="_blank" href="<?=base_url()?>home/order/<?=$d->link_unique;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-purple dz-tip" title="Show on Customer Page"> <i class="fa fa-search"></i> </a>
						<a href="<?=base_url()?>manage/order/delete/<?=$d->id;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-danger dz-tip confirm-delete" title="Delete Order"> <i class="fa fa-trash"></i> </a>
						<?php if ($d->receipt != ""): ?>
							<br><br>
							<a href="<?=base_url()?>assets/receipt/<?=$d->receipt;?>"
								title="show customer payment confirmation"
								class="btn-confirm-download btn btn-success waves-effect waves-light btn-sm dz-tip" target="_blank"> 
								<i class="fa fa-file-image-o m-r-5"></i> 
								<span>payment<br>confirmation</span> 
							</a>													
						<?php endif ?>
						
					</div>
				</td>
			</tr>
		<?endforeach;?>
	</tbody>
</table>