<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Pop Up Ads</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <?php if ($this->sub_domain == "all"): ?>
        
            <div class="row">                                
                <div class="col-md-12">
                    <div class="card">
                    <center>
                        <br>
                        <h3>please select sub domain above</h3>
                        <br>
                    </center>
                    </div>
                </div>
            </div>

        <?php else: ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title m-t-0">Modify Image Pop Ads</h4>
                            <br>

                            <?php if ($items->value == ""): ?>
                                <center>
                                    <b>no active pop up ads</b>
                                </center>
                            <?php else: ?>
                                <center>
                                    <a href="<?=base_url()?>assets/popads/<?=$items->value;?>" target="_blank">
                                        <img style="width:100px" src="<?=base_url()?>assets/popads/<?=$items->value;?>">
                                    </a>
                                    <div class="clear"></div><br>
                                    <a href="<?=base_url('manage/popads/delete')?>">delete ads</a>
                                </center>
                            <?php endif ?>

                            <br>
                            <hr>
                            <br>

                            <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">Upload Image</label>
                                    <div class="col-md-10">
                                        <input type="file" class="default" name="userfile">
                                        <br>
                                        <p class="text-muted m-b-25">* JPG & PNG allowed.</p>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>