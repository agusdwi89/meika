<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Footer</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Footer Section</h4>
                        <br>
                        <?=form_open('',array("class"=>"form-horizontal"))?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-12 control-label"><h4>Map Location</h4></label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Latitude</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="Left Title" class="form-control" name="latitude" value="<?=$items->latitude;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Longitude</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="Left Title" class="form-control" name="longitude" value="<?=$items->longitude;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Contact Us Title</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="Left Title" class="form-control" name="c_title" value="<?=$items->c_title;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Contact Us Description</label>
                                    <div class="col-md-9">
                                        <textarea placeholder="Description text" class="form-control" rows="5" name="c_description"><?=$items->c_description;?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-12 control-label"><h4>Address</h4></label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="Address Text" class="form-control" name="r_address" value="<?=$items->r_address;?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Phone Number</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="Phone number" class="form-control" name="r_phone" value="<?=$items->r_phone;?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Email Address</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="Email Text" class="form-control" name="r_email" value="<?=$items->r_email;?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Footer Text</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="Footer Text" class="form-control" name="footer_text" value="<?=$items->footer_text;?>">
                                    </div>
                                </div>

                            </div>
                        </div>

                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>