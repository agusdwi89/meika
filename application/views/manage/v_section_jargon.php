<style type="text/css">
    #section_internal, #section_eksternal{display: none;}
</style>
<script type="text/javascript">
    $(function(){
        $("#section_internal").show();

        $( "#opt_link" ).change(function() {
            if ($(this).val() == "Internal") {
                $("#section_eksternal").hide();
                $("#section_internal").show();
            }else{
                $("#section_internal").hide();
                $("#section_eksternal").show();
            }
        });
        $( "#opt_link" ).trigger('change');
    })
</script>   

<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Jargon</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Jargon Section</h4>
                        <br>

                        <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">Title</label>
                                        <div class="col-md-5">
                                            <input type="text" placeholder="first title" class="form-control" name="def[title]" value="<?=$items->title;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">Description</label>
                                        <div class="col-md-10">
                                            <input type="text" placeholder="second title" class="form-control" name="def[description]" value="<?=$items->description;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">Button Text</label>
                                        <div class="col-md-5">
                                            <input type="text" name="def[button_text]" class="form-control" value="<?=$items->button_text;?>" placeholder="Button text">
                                            <small>*leave blank if you want to remove this button</small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">Link</label>
                                        <div class="col-md-5">
                                            <select id="opt_link" name="def[link]" class=" form-control" title="">
                                                <option <?=("Internal" == $items->link ? 'selected="selected"' : '' )?> >Internal</option>
                                                <option <?=("eksternal" == $items->link ? 'selected="selected"' : '' )?> >Eksternal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 control-label">URL</label>
                                        <div class="col-md-5">
                                            <div id="section_internal">
                                                <select name="Internal" class=" form-control" title="">
                                                    <?php foreach ($section->result() as $s): ?>
                                                        <option <?=($s->section_name == $items->url ? 'selected="selected"' : '' )?>><?=$s->section_name;?></option>
                                                    <?php endforeach ?>
                                                </select>   
                                            </div>
                                            <div id="section_eksternal">
                                                <input type="text" placeholder="eksternal url" class="form-control" name="Eksternal" value="<?=$items->url;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">image</label>
                                        <div class="col-md-10">
                                            <?if ($items->image != ""): ?>
                                            <a target="_blank" href="<?=base_url()?>assets/section/<?=$items->image;?>">
                                                <img class="image-section-header-placeholder" src="<?=base_url()?>assets/section/<?=$items->image;?>">
                                            </a>
                                            <span>change image : </span>
                                            <?endif;?>
                                            <input type="file" class="default" name="userfile[]">
                                            <br>
                                            <p class="text-muted m-b-25">* Image size 1920 x 402 px , JPG & PNG allowed.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>