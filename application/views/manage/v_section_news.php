<style type="text/css">
    #section_internal, #section_eksternal{display: none;}
</style>
<script type="text/javascript">
    $(function(){
        $("#section_internal").show();

        $( "#opt_link" ).change(function() {
            if ($(this).val() == "Internal") {
                $("#section_eksternal").hide();
                $("#section_internal").show();
            }else{
                $("#section_internal").hide();
                $("#section_eksternal").show();
            }
        });
        $( "#opt_link" ).trigger('change');
    })
</script>   

<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage News</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit News Section</h4>
                        <br>

                        <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-md-12 control-label">Title (Home Page)</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="first title" class="form-control" name="def[title]" value="<?=$items->title;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-12 control-label">Description (Home Page)</label>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="description" class="form-control" name="def[description]" value="<?=$items->description;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-12">BG image (detail post)</label>
                                        <div class="col-md-12">
                                            <?if ($items->image != ""): ?>
                                            <a target="_blank" href="<?=base_url()?>assets/section/<?=$items->image;?>">
                                                <img class="image-section-header-placeholder" src="<?=base_url()?>assets/section/<?=$items->image;?>">
                                            </a>
                                            <span>change image : </span>
                                            <?endif;?>
                                            <input type="file" class="default" name="userfile[]">
                                            <br>
                                            <p class="text-muted m-b-25">* Image size 1920 x 402 px , JPG & PNG allowed.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>