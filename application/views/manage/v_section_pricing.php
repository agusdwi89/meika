<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Pricing</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Pricing Section</h4>
                        <br>
                        <?=form_open('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Title</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="title first" class="form-control" name="title" value="<?=$master->title;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Description</label>
                                <div class="col-md-12">
                                    <textarea placeholder="Description text" class="form-control" rows="5" name="description"><?=$master->description;?></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Add Price List</h4>
                        <br>
                        <?=form_open_multipart('manage/section_pricing/add_item',array("class"=>"form-horizontal"))?>
                            <input type="hidden" name="pricing_id" value="<?=$id;?>"> 
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="title text" class="form-control" name="name" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Price ( text )</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="price text" class="form-control" name="price" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Facility</label>
                                <div class="col-md-10">
                                    <textarea style="min-height:30px;height:50px" placeholder="Description text" class="form-control" rows="1" name="lists"></textarea>
                                    <p class="text-muted">*separate with coma each facility</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Photo</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile[]">
                                    <p class="text-muted">* Image size up to 270 x 171 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-reload" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Clear</button>
                        <?=form_close()?>
                        <br>
                        <table class="table table-space m-0">   
                            <thead>
                                <tr>
                                    <th style="text-align:center">Photo</th>
                                    <th style="text-align:center">Name</th>
                                    <th style="text-align:center">Price</th>
                                    <th style="text-align:center">Facility</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?foreach ($items->result() as $k):?>
                                    <tr>
                                        <td>
                                            <a href="<?=base_url()?>assets/section/<?=$k->image;?>" target="_blank">
                                                <img width=100 src="<?=base_url()?>assets/section/<?=$k->image;?>">
                                            </a>
                                        </td>
                                        <td><b><?=$k->name;?></b></td>
                                        <td><b><?=$k->price;?></b></td>
                                        <td>
                                            <?$f = json_decode($k->lists);?>
                                            <?foreach ($f as $ff): ?>
                                                <span class="badge badge-info"><?=$ff;?></span>
                                            <?endforeach;?>
                                        </td>
                                        <td style="text-align:center">
                                            <a style="opacity:100 !important;float:none" title="delete" href="<?=base_url()?>manage/section_pricing/delete_item/<?=$k->id;?>/<?=$id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>