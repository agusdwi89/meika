<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Reservation Section</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Reservation Section</h4>
                        <br>
                        <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Title</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="title text" class="form-control" name="ar[title]" value="<?=$items->title;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Sub Title</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="sub title" class="form-control" name="ar[subtitle]" value="<?=$items->subtitle;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Tagline</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Tagline" class="form-control" name="ar[tagline]" value="<?=$items->tagline;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <textarea placeholder="Description text" class="form-control" rows="5" name="ar[description]"><?=$items->description;?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Reserv Button Text</label>
                                <div class="col-md-5">
                                    <input type="text" name="ar[reservation_button_text]" class="form-control" value="<?=$items->reservation_button_text;?>" placeholder="Reservation Button text eg:Reservation">
                                    <small>*leave blank if you want to remove this button</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Button Text</label>
                                <div class="col-md-5">
                                    <input type="text" name="ar[button_text]" class="form-control" value="<?=$items->button_text;?>" placeholder="Button text">
                                    <small>*leave blank if you want to remove this button</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Button Type</label>
                                <div class="col-md-5">
                                    <select id="select-type" name="ar[link]" class=" form-control" title="">
                                        <option <?=(("internal" == $items->link)?"selected='selected'":"");?> value="internal">Internal</option>
                                        <option <?=(("eksternal" == $items->link)?"selected='selected'":"");?> value="eksternal">External</option>
                                    </select>                                
                                </div>
                            </div>
                            <div id="state-section" class="form-group row state-option">
                                <label class="col-md-2 control-label">Section Link</label>
                                <div class="col-md-5">
                                    <select name="state[internal]" class=" form-control" title="">
                                        <?php foreach ($sectionopt->result() as $s): ?>
                                            <option <?=(($s->section_name == $items->url)?"selected='selected'":"");?>><?=$s->section_name;?></option>
                                        <?php endforeach ?>
                                    </select>                                
                                </div>
                            </div>
                            <div id="state-external" class="form-group row state-option">
                                <label class="col-md-2 control-label">External URL</label>
                                <div class="col-md-5">
                                    <input type="text" name="state[eksternal]" class="form-control" value="<?=$items->url;?>" placeholder="http://www....">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">image</label>
                                <div class="col-md-10">
                                    <?if ($items->image != ""): ?>
                                        <a target="_blank" href="<?=base_url()?>assets/section/<?=$items->image;?>">
                                            <img class="image-section-header-placeholder" src="<?=base_url()?>assets/section/<?=$items->image;?>">
                                        </a>
                                        <span>change image : </span>
                                    <?endif;?>
                                    <input type="file" class="default" name="userfile">
                                    <br>
                                    <p class="text-muted m-b-25">* Image size 585 x 409 px , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Form Title (Pop Up)</label>
                                <div class="col-md-5">
                                    <input type="text" name="ar[form_title]" class="form-control" value="<?=$items->form_title;?>" placeholder="Title text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Form Description (Pop Up)</label>
                                <div class="col-md-5">
                                    <input type="text" name="ar[form_description]" class="form-control" value="<?=$items->form_description;?>" placeholder="Description text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Form Footer Main</label>
                                <div class="col-md-5">
                                    <input type="text" name="ar[form_footer_main]" class="form-control" value="<?=$items->form_footer_main;?>" placeholder="Title text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Form Footer Secondary</label>
                                <div class="col-md-5">
                                    <input type="text" name="ar[form_footer_secondary]" class="form-control" value="<?=$items->form_footer_secondary;?>" placeholder="Description text">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function(){
        $(".state-option").hide();
        $( "#select-type" ).change(function(e) {
           if ($(this).val() == "internal") {
                $("#state-external").hide();
                $("#state-section").show();
            }else{
                $("#state-section").hide();
                $("#state-external").show();
            }
        });
        $( "#select-type" ).trigger('change');
    })
</script>