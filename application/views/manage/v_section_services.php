<script type="text/javascript">
    $(function(){
        $('body').on('click','.icon-choose-technology i',function (e) {
            e.preventDefault();
            econ = $(this);
            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');
            $("#input-hidden-icon").val($(this).data('icn'));
        });
    })
</script>

<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Services</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Services Section</h4>
                        <br>
                        <?=form_open('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Title</label>
                                <div class="col-md-10">
                                    <input type="text" placeholder="title text" class="form-control" name="title" value="<?=$master->title;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Sub Title</label>
                                <div class="col-md-10">
                                    <input type="text" placeholder="subtitle text" class="form-control" name="subtitle" value="<?=$master->subtitle;?>">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Services Items</h4>
                        <br>
                        <?=form_open_multipart('manage/section_services/add_item',array("class"=>"form-horizontal"))?>
                            <input type="hidden" name="services_id" value="<?=$id;?>"> 
                            <input id="input-hidden-icon" type="hidden" name="icon" value=""> 
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Title</label>
                                <div class="col-md-9">
                                    <input type="text" placeholder="title text" class="form-control" name="title" value="">
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <label class="col-md-3 control-label">Price</label>
                                <div class="col-md-9">
                                    <input type="number" placeholder="title text" class="form-control" name="price" value="">
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Description</label>
                                <div class="col-md-9">
                                    <textarea style="min-height:50px;height:50px" placeholder="Description text" class="form-control" rows="1" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Image</label>
                                <div class="col-md-9">
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted m-b-25">* Image size up to 370 x 192 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 control-label">Icon</label>
                                <div class="col-md-9">
                                    <div class="icon-choose-technology" style="height: 300px; overflow: auto;">
                                        <?php foreach ($iconset as $k): ?>
                                            <i data-icn="<?=$k;?>" class="<?=$k;?>"></i>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-reload" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Clear</button>
                        <?=form_close()?>
                        <br>
                        <table class="table table-space m-0">   
                            <thead>
                                <tr>
                                    <th>Ico</th>
                                    <th>Image</th>
                                    <th>Title / Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?foreach ($items->result() as $k):?>
                                    <tr>
                                        <td>
                                            <i style="font-size:25px" class="<?=$k->icon?>"></i>
                                        </td>
                                        <td>
                                            <img width="75" src="<?=base_url()?>assets/section/<?=$k->image;?>">
                                        </td>
                                        <td>
                                            <b><?=$k->title;?></b>
                                            <p><?=$k->description;?></p>
                                        </td>
                                        <td>
                                            <center>
                                                <a style="opacity:100 !important;margin-right:12px" title="delete" href="<?=base_url()?>manage/section_services/delete_item/<?=$k->id;?>/<?=$id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                            </center>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>