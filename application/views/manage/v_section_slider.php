<style type="text/css">
    #section_internal, #section_eksternal{display: none;}
</style>
<script type="text/javascript">
    $(function(){
        $("#section_internal").show();

        $( "#opt_link" ).change(function() {
            if ($(this).val() == "Internal") {
                $("#section_eksternal").hide();
                $("#section_internal").show();
            }else{
                $("#section_internal").hide();
                $("#section_eksternal").show();
            }
        });
    })
</script>   

<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Slider</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Add Slider Photo</h4>
                        <br>
                        <?=form_open_multipart('manage/section_slider/add_item',array("class"=>"form-horizontal"))?>
                            <input type="hidden" name="def[slider_id]" value="<?=$id;?>"> 
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Title</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="title text" class="form-control" name="def[title1]" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Highlight Title</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="highlight title text" class="form-control" name="def[title2]" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <textarea style="min-height:30px;height:30px" placeholder="Description text" class="form-control" rows="1" name="def[description]"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Button Text</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Button Text eg: Read More" class="form-control" name="def[button_text]" value="">
                                    <small>*Leave blank to remove a button</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Link</label>
                                <div class="col-md-5">
                                    <select id="opt_link" name="def[link]" class=" form-control" title="">
                                        <option>Internal</option>
                                        <option>Eksternal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">URL</label>
                                <div class="col-md-5">
                                    <div id="section_internal">
                                        <select name="Internal" class=" form-control" title="">
                                            <?php foreach ($section->result() as $s): ?>
                                                <option><?=$s->section_name;?></option>
                                            <?php endforeach ?>
                                        </select>   
                                    </div>
                                    <div id="section_eksternal">
                                        <input type="text" placeholder="eksternal url" class="form-control" name="Eksternal" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Photo</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile[]">
                                    <p class="text-muted m-b-25">* Image size up to 1920 x 1050 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                        <br>
                        <table class="table table-space m-0">   
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Title - Secondary - Description</th>
                                    <th>Link URL</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?foreach ($items->result() as $k):?>
                                    <tr>
                                        <td>
                                            <a href="<?=base_url()?>assets/section/<?=$k->image;?>" target="_blank">
                                                <img width=100 src="<?=base_url()?>assets/section/<?=$k->image;?>">
                                            </a>
                                        </td>
                                        <td>
                                            <b><?=$k->title1;?> | <?=$k->title2;?></b>
                                            <p><?=$k->description;?></p>
                                        </td>
                                        <td>
                                            <?=$k->link;?><br>
                                            <?=$k->url;?>
                                        </td>
                                        <td>
                                            <center>
                                                <a style="opacity:100 !important;margin-right:12px" title="delete" href="<?=base_url()?>manage/section_slider/delete_item/<?=$k->id;?>/<?=$id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                            </center>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>