<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Team</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Team Section</h4>
                        <br>
                        <?=form_open('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Title</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="title first" class="form-control" name="title" value="<?=$master->title;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Description</label>
                                <div class="col-md-12">
                                    <textarea placeholder="Description text" class="form-control" rows="5" name="description"><?=$master->description;?></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Add Team Member</h4>
                        <br>
                        <?=form_open_multipart('manage/section_team/add_item',array("class"=>"form-horizontal"))?>
                            <input type="hidden" name="steam_id" value="<?=$id;?>"> 
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="title text" class="form-control" name="name" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Position</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="position text" class="form-control" name="position" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <textarea style="min-height:30px;height:50px" placeholder="Description text" class="form-control" rows="1" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Social</label>
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-facebook"></i></span>
                                        </div>
                                        <input type="text" name="social[facebook]" class="form-control" placeholder="facebook username">
                                    </div>
                                    <br>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-twitter"></i></span>
                                        </div>
                                        <input type="text" name="social[twitter]" class="form-control" placeholder="twitter username">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-instagram"></i></span>
                                        </div>
                                        <input type="text" name="social[instagram]" class="form-control" placeholder="instagram username">
                                    </div>
                                    <br>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-linkedin"></i></span>
                                        </div>
                                        <input type="text" name="social[linkedin]" class="form-control" placeholder="linkedin username">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Photo</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile[]">
                                    <p class="text-muted m-b-25">* Image size up to 610 x 610 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-reload" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Clear</button>
                        <?=form_close()?>
                        <br>
                        <table class="table table-space m-0">   
                            <thead>
                                <tr>
                                    <th style="text-align:center">Photo</th>
                                    <th style="text-align:center">Name / Position</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?foreach ($items->result() as $k):?>
                                    <tr>
                                        <td>
                                            <a href="<?=base_url()?>assets/section/<?=$k->photo;?>" target="_blank">
                                                <img width=100 src="<?=base_url()?>assets/section/<?=$k->photo;?>">
                                            </a>
                                        </td>
                                        <td>
                                            <b><?=$k->name;?> | <?=$k->position;?></b>
                                            <p><?=$k->description;?></p>
                                        </td>
                                        <td style="text-align:center">
                                            <?$socials = json_decode($k->social);?>
                                            <a target="_blank" style="opacity:100 !important;float:none;margin-right:5px" title="facebook" href="http://facebook.com/<?=$socials->facebook;?>" class="fa fa-facebook dz-tip"></a>
                                            <a target="_blank" style="opacity:100 !important;float:none;margin-right:5px" title="twitter" href="http://twitter.com/<?=$socials->twitter;?>" class="fa fa-twitter dz-tip"></a>
                                            <a target="_blank" style="opacity:100 !important;float:none;margin-right:5px" title="instagram" href="http://instagram.com/<?=$socials->instagram;?>" class="fa fa-instagram dz-tip"></a>
                                            <a target="_blank" style="opacity:100 !important;float:none;margin-right:5px" title="linkedin" href="http://linkedin.com/<?=$socials->linkedin;?>" class="fa fa-linkedin dz-tip"></a>
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a style="opacity:100 !important;float:none" title="delete" href="<?=base_url()?>manage/section_team/delete_item/<?=$k->id;?>/<?=$id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>