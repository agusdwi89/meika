<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Video</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Video Section</h4>
                        <br>
                        <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Title Text</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Title" class="form-control" name="title" value="<?=$items->title;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Youtube Video Code</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="youtube video code" class="form-control" name="link" value="<?=$items->link;?>">
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <p style="margin-top:10px">input youtube video code (red color)<br>example : https://www.youtube.com/watch?v=<b style="color:#f33f3f">Amq-qlqbjYA</b></p>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-12">Image</label>
                                <div class="col-md-12">
                                    <?if ($items->image != ""): ?>
                                    <a target="_blank" href="<?=base_url()?>assets/section/<?=$items->image;?>">
                                        <img class="image-section-header-placeholder" src="<?=base_url()?>assets/section/<?=$items->image;?>">
                                    </a>
                                    <span>change image : </span>
                                    <?endif;?>
                                    <input type="file" class="default" name="userfile[]">
                                    <br>
                                    <p class="text-muted m-b-25">* Image size 1920 x 402 px , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Preview Video</h4>
                        <br>
                        <?php if ($items->link != ""): ?>
                            <iframe width="420" height="315"src="https://www.youtube.com/embed/<?=$items->link;?>"></iframe>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>