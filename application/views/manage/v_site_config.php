<? $tb = !empty($this->session->flashdata('tab')) ? $this->session->flashdata('tab') : 'global' ;?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Site Configuration</h4>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <?php if ($this->sub_domain == "all"): ?>
                <div class="col-md-12">
                    <div class="card">
                        <center>
                            <br><br>
                            <h3>please select sub domain above</h3>
                            <br><br>
                        </center>
                    </div>
                </div>
            <?php else: ?>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?=($tb == "global")? 'active' : '' ?>" id="global-tab" data-toggle="tab" href="#global" role="tab" aria-controls="global" aria-selected="true">
                                        <span class="d-block d-sm-none"><i class="fa fa-home"></i></span>
                                        <span class="d-none d-sm-block">Global Config</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?=($tb == "logo")? 'active' : '' ?>" id="logo-tab" data-toggle="tab" href="#logo" role="tab" aria-controls="logo" aria-selected="false">        
                                        <span class="d-block d-sm-none"><i class="fa fa-user"></i></span>
                                        <span class="d-none d-sm-block">Logo</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?=($tb == "pure")? 'active' : '' ?>" id="pure-tab" data-toggle="tab" href="#pure" role="tab" aria-controls="pure" aria-selected="false">
                                        <span class="d-block d-sm-none"><i class="fa fa-envelope-o"></i></span>
                                        <span class="d-none d-sm-block">Purechat</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane <?=($tb == "global")? 'show active' : '' ?>" id="global" role="tabpanel" aria-labelledby="global-tab">
                                    <?=form_open()?>
                                    <?foreach ($data as $key => $value): ?>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label"><?=$key;?></label>
                                        <div class="col-md-9">
                                            <?php if ($key == "google-track" || $key == "fb-track"): ?>
                                                <textarea style="font-size:10px" class="form-control" name="<?=$key;?>"><?=$value;?></textarea>
                                            <?php else: ?>
                                                <input type="text" placeholder="title first" class="form-control" name="<?=$key;?>" value="<?=$value;?>">
                                            <?php endif ?>
                                        </div>
                                    </div>
                                    <?endforeach;?>
                                    <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                    <?=form_close()?>      
                                </div>
                                <div class="tab-pane <?=($tb == "logo")? 'show active' : '' ?>" id="logo" role="tabpanel" aria-labelledby="logo-tab">
                                    <small>*If logo didnt change after upload, try clear cache or check live website on incognito mode, image need refresh from browser cache</small>
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <?=form_open_multipart(base_url('manage/site_config/set_logo1'))?>
                                                <div class="form-group row">
                                                    <label class="col-md-3 control-label">Primary Logo</label>
                                                    <div class="col-md-9">
                                                        <a target="_blank" href="<?=base_url()?>assets/qs/images/logo/logo_1.png">
                                                            <img style="width:200px !important" class="image-section-header-placeholder" src="<?=base_url()?>assets/qs/images/logo/logo_1.png">
                                                        </a>
                                                        <br><br>
                                                        <span>change image : </span>
                                                        <input type="file" class="default" name="userfile">
                                                        <br>
                                                        <p class="text-muted m-b-25">* Image size 168 x 50 px , PNG allowed.</p>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                            <?=form_close()?>         
                                        </div>
                                        <div class="col-md-4">
                                            <?=form_open_multipart(base_url('manage/site_config/set_logo'))?>
                                                <div class="form-group row">
                                                    <label class="col-md-3 control-label">Black Logo</label>
                                                    <div class="col-md-9">
                                                        <a target="_blank" href="<?=base_url()?>assets/qs/images/logo/logo.png">
                                                            <img style="width:200px !important" class="image-section-header-placeholder" src="<?=base_url()?>assets/qs/images/logo/logo.png">
                                                        </a>
                                                        <br><br>
                                                        <span>change image : </span>
                                                        <input type="file" class="default" name="userfile">
                                                        <br>
                                                        <p class="text-muted m-b-25">* Image size 168 x 50 px , PNG allowed.</p>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                            <?=form_close()?>         
                                        </div>
                                        <div class="col-md-4">
                                            <?=form_open_multipart(base_url('manage/site_config/set_logo2'))?>
                                                <div class="form-group row">
                                                    <label class="col-md-3 control-label">White Logo</label>
                                                    <div class="col-md-9">
                                                        <a target="_blank" href="<?=base_url()?>assets/qs/images/logo/logo-black.png">
                                                            <img style="width:200px !important" class="image-section-header-placeholder" src="<?=base_url()?>assets/qs/images/logo/logo-black.png">
                                                        </a>
                                                        <br><br>
                                                        <span>change image : </span>
                                                        <input type="file" class="default" name="userfile">
                                                        <br>
                                                        <p class="text-muted m-b-25">* Image size 168 x 50 px , PNG allowed.</p>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                            <?=form_close()?>         
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane <?=($tb == "pure")? 'show active' : '' ?>" id="pure" role="tabpanel" aria-labelledby="pure-tab">
                                    <?=form_open(base_url('manage/site_config/set_pure'))?>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Pure Chat Code</label>
                                        <div class="col-md-9">
                                            <textarea style="font-size:10px" class="form-control" name="value"><?=$value_pure;?></textarea>
                                            <input type="hidden" name="id" value="1001">
                                        </div>
                                        </div>
                                        <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                    <?=form_close()?>      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>