<!DOCTYPE html>
<html lang="en">
	<head>
		<?=$this->load->view('fe/header');?>	
		<script type="text/javascript">
			var clog;
		</script>
	</head>

<body>
	<?=form_open('/',array('id'=>'global-form'))?>
	<?=form_close()?>

	<!-- Start Preloader -->
	<div id="page-preloader">
		<svg class="circular" height="50" width="50">
			<circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"></circle>
		</svg>
	</div>
	<!-- End Preloader -->

	<!-- Start Content -->
	<section id="main" class="wrapper">
		<?=$this->load->view('fe/nav2');?>

		<section id="news-thumb" class="section border-0 m-0 pb-3 reviews block">
			<div class="container container-lg">
				<div class="row pb-1">

					<h2>News</h2>
					<div class="block-desc">Dapatkan berita / kabar terbaru dari kami</div>

					<?php foreach ($news->result() as $n): ?>
						<div class="col-sm-6 col-lg-4 mb-4 pb-2">
							<a href="<?=base_url()?>news/<?=$n->slug;?>">
								<article>
									<div class="thumb-info thumb-info-no-borders thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom border-radius-0">
										<div class="thumb-info-wrapper thumb-info-wrapper-opacity-6">
											<img src="<?=base_url()?>assets/news/<?=$n->img_thumbnail;?>" class="img-fluid" alt="<?=$n->title;?>">
											<div class="dates"><?=pretty_date($n->date,false);?></div>
											<div class="thumb-info-title bg-transparent p-4">
												<div class="thumb-info-inner mt-1">
													<h2 class="text-color-light line-height-2 text-4 font-weight-bold mb-0"><?=$n->title;?></h2>
												</div>
												<div class="thumb-info-show-more-content">
													<p class="mb-0 text-1 line-height-9 mb-1 mt-2 text-light opacity-5"><?=smart_trim($n->content,150);?></p>
												</div>
											</div>
										</div>
									</div>
								</article>
							</a>
						</div>	
					<?php endforeach ?>

				</div>
			</div>
		</section>

		<?php foreach ($list_section as $key): ?>

			<?=$this->load->view('section/'.$key['layout'],$key);?>
			
		<?php endforeach ?>


	</section>
	<!-- End Content -->
	<?=$this->load->view('fe/footer_script');?>
	<?=$this->load->view('fe/i_popup');?>

	<?=$this->load->view('fe/ga');?>

</html>