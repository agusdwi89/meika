<!DOCTYPE html>
<html lang="en">
	<head>
		<?=$this->load->view('fe/header');?>	
		<script type="text/javascript">
			var clog;
		</script>
		<meta name="keywords" content="<?=$item->meta_keyword;?>" />
		<meta name="description" content="<?=$item->meta_description;?>">
	</head>
<body>
	<?=form_open('/',array('id'=>'global-form'))?>
	<?=form_close()?>

	<!-- Start Preloader -->
	<div id="page-preloader">
		<svg class="circular" height="50" width="50">
			<circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"></circle>
		</svg>
	</div>
	<!-- End Preloader -->

	<!-- Start Content -->
	<section id="main" class="wrapper">
		<?=$this->load->view('fe/nav2');?>

		<section id="news-thumb" class="section border-0 m-0 pb-3 reviews block">
			<div class="container container-lg">
				<div class="row pb-1">
					<h2><?=$item->title;?></h2>
					<div class="block-desc"><?=pretty_date($item->date, false);?></div>
				</div>
				<div class="row">
					<div class="col">
						<div class="blog-posts single-post">
							<article class="post post-large blog-single-post border-0 m-0 p-0">
								<div class="post-image ml-0">
									<a href="<?=base_url()?>assets/news/<?=$item->img_header;?>" target="_blank">
										<img src="<?=base_url()?>assets/news/<?=$item->img_header;?>" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="">
									</a>
								</div>
								<br>
								<div class="post-content ml-0">
									<div>
										<?=$item->content;?>
									</div>

									<div class="post-block mt-5 post-share">
										<h4 class="mb-3">Share this Post</h4>

										<!-- AddThis Button BEGIN -->
										<div class="addthis_toolbox addthis_default_style ">
											<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
											<a class="addthis_button_tweet"></a>
											<a class="addthis_button_pinterest_pinit"></a>
											<a class="addthis_counter addthis_pill_style"></a>
										</div>
										<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
										<!-- AddThis Button END -->

									</div>
								</div>
							</article>
						</div>
					</div>
				</div>
				<div class="row">
					<center>
						<a href="<?=base_url('news')?>">
							<h4>Index Berita</h4>
						</a>
					</center>
					<br>
					
					<?
					/*
					<div id="disqus_thread"></div>
					<script>
						var disqus_config = function () {
							this.page.url = "<?=current_url();?>";
							this.page.identifier = "<?=$item->slug;?>"; 
						};
						(function() { 
							var d = document, s = d.createElement('script');
							s.src = 'https://EXAMPLE.disqus.com/embed.js';
							s.setAttribute('data-timestamp', +new Date());
							(d.head || d.body).appendChild(s);
						})();
					</script>
					<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
					*/
					?>
				</div>
			</div>
		</section>


		<?php foreach ($list_section as $key): ?>

			<?=$this->load->view('section/'.$key['layout'],$key);?>
			
		<?php endforeach ?>


	</section>
	<!-- End Content -->
	<?=$this->load->view('fe/footer_script');?>
	<?=$this->load->view('fe/i_popup');?>

	<?=$this->load->view('fe/ga');?>

</html>