<!-- Meta Tag -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?=$this->db->get_where('site_config',array('id'=>4))->row()->value;?>">
<meta name="keywords" content="<?=$this->db->get_where('site_config',array('id'=>5))->row()->value;?>">
<meta name="author" content="meika">
<title><?=$this->db->get_where('site_config',array('id'=>1))->row()->value;?></title>

<!-- Links of CSS Files -->
<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/settings.css">
<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/style.css">
<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/responsive.css">

<?$color_theme = $this->db->get_where('site_config',array('subdomain' => subdomain(), 'name'=>'theme'))->row();?>
<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/colors/<?=$color_theme->value;?>.css" id="color-change">

<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/default-animate.css">
<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/font-awesome.css">
<link rel="stylesheet" href="<?=base_url()?>assets/qs/fonts/flaticon.css">
<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/bootstrap-formhelpers.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/loader.css">
<link rel="stylesheet" href="<?=base_url()?>assets/fe/css/simple-line-icons.css">