<script type="text/javascript">
	var MAP_LONG 	= "<?=$data->longitude;?>";
	var MAP_LAT 	= "<?=$data->latitude;?>";
</script>

<!--============== Start Map and Contact Form Section ====-->
<section id="footer" data-anchor="map_contact">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div id="map" class="map-canvas wow animated slideInUp"></div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="section_title wow animated slideInUp">
					<h2><?=$data->c_title;?></h2>
					<p><?=$data->c_description;?></p>
				</div>
				<?=form_open(base_url('home/reserv'),array('class'=>'contact_message','id'=>'contact-form'))?>
					<div class="form-group col-md-6 col-sm-6">
						<input class="form-control no-border" type="text" name="name" placeholder="Name"/>
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<input class="form-control no-border" type="text" name="email" placeholder="Email*"/>
					</div>
					<div class="form-group col-md-12 col-sm-12">
						<input class="form-control no-border" id="subject" type="text" name="subject" placeholder="Subject*"/>
					</div>
					<div class="form-group col-md-12 col-sm-12">
						<textarea class="form-control no-border" id="message" name="message" placeholder="Message*"></textarea>
					</div>
					<div class="form-group col-md-12 col-sm-6">
						<input class="btn_df btn_dafault" id="send" type="submit" value="send"/>
					</div>
					<div class="col-md-12">
						<div class="error-handel">
							<div id="success">Your email sent Successfully, Thank you.</div>
							<div id="error"> Error occurred while sending email. Please try again later.</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<!--=========== Map and Contact Form Section End ============-->
<!--=========== Start Contact Info Section ==================-->
<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="contact_info wow animated slideInUp">
					<div class="contact_icon"><i class="fa fa-map-marker"></i></div>
					<div class="info">
						<h4>Address :</h4>
						<p><?=$data->r_address;?></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="contact_info wow animated slideInUp">
					<div class="contact_icon"><i class="fa fa-phone"></i></div>
					<div class="info">
						<h4>Phone Number :</h4>
						<p>Phone : <?=$data->r_phone;?></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="contact_info wow animated slideInUp">
					<div class="contact_icon"><i class="fa fa-envelope-o"></i></div>
					<div class="info">
						<h4>Email :</h4>
						<p><?=$data->r_email;?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--========== End Contact Info Section ===========-->
<!--========== Start Footer =======================-->
<footer id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="coppyright_area">
					<?=$data->footer_text;?>
				</div>
			</div>
		</div>
	</div>
</footer>