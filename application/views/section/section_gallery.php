<?$items = $this->db->get_where('section_gallery_items',array('sgal_id'=>$data->id));?>

<?
	$q 	= "select tag from section_gallery_items GROUP BY tag";
	$qq = $this->db->query($q);
	$d  = array();

	foreach ($qq->result() as $v) {
		$l = explode(",", $v->tag);
		foreach ($l as $val) {
			$d[$val] = 1;
		}
	}
	$tag = array();
	foreach ($d as $key => $value) {
		$tag[] = $key;
	}
?>

<!--============== Start Our Gallery Section =========-->
<section id="gallery" class="py_80" data-anchor="our_gallery">
	<div class="container-fluid">
		<div class="row">
			<div class="gallery-section">
			   <div class ="col-md-12 col-sm-12 col-xs-12">
				  <div class="section_title">
					  <h2><?=$data->title;?></h2>
					  <p><?=$data->description;?></p>
				  </div>
			   </div>
				   <!-- Tab Gallery -->
				<div class="filters text-center">
					<ul class="filter-tabs filter-btns clearfix anim-3-all">
						<li class="active filter" data-role="button" data-filter="all">All</li>	
						<?foreach ($tag as $t): ?>
							<li class="filter" data-role="button" data-filter=".<?=$t;?>"><?=$t;?></li>	
						<?endforeach;?>
					</ul>
				</div>
					
				<div class="filter-list clearfix">
					<div class="portfolio-items">                           
						<?foreach ($items->result() as $g): ?>
							<div class="column mix mix_all <?=str_replace(",", " ", $g->tag);?> col-md-3 col-sm-3 col-xs-12">
								<div class="row">
									<div class="default-portfolio-item">
										<a href="<?=base_url()?>assets/section/<?=$g->image;?>" data-fancybox="fancy_gallery">
											<img src="<?=base_url()?>assets/section/<?=$g->image;?>" alt="image" />
											<div class="overlay-box">
												<span><i class="fa fa-plus-circle"></i></span>
											</div>
										</a>
									</div>
								</div>
							</div>
						<?endforeach;?>
					</div>
				</div>
			   <!-- End Tab Gallery -->
			</div>
		</div>
	</div>
</section>
<!--============== End Our Gallery Section ================-->