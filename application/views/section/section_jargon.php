<!--============== Start Join us Section=================-->
<div id="jargon" class="py_80 bgoverlay" style="background:url(<?=base_url()?>assets/section/<?=$data->image;?>)">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="join_content wow animated slideInLeft">
					<h3><?=$data->title;?></h3>
					<p><?=$data->description;?></p>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="join_btn wow animated slideInRight">
					<?php if (strlen($data->button_text) > 0): ?>
						<?if ($data->link == "internal"): ?>
							<a class="btn_df btn_dafault" href="#<?=$data->url;?>">read more</a>
						<?else:?>
							<a class="btn_df btn_dafault" target="_blank" href="<?=$data->url;?>">read more</a>
						<?endif;?>
					<?php endif ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!--============== End Join us Section=================-->