<? $news = $this->db->limit(5)->get_where('news',array('subdomain'=>subdomain()));?>
<!--============ Start Our Blog Section ===============-->
<section id="news" class="py_80" data-anchor="our_blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="section_title wow animated slideInUp">
					<h2><?=$data->title;?></h2>
					<p><?=$data->description;?></p>
				</div>
			</div>

			<?foreach ($news->result() as $n): ?>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="blog_thumb wow animated slideInUp">
						<div class="blog_image">
							<img src="<?=base_url()?>assets/news/<?=$n->img_thumbnail;?>" alt="Blog image 1">
							<div class="blog_overlay"></div>
						</div>
						<div class="blog_data">
							<span class="date"><?=format_date_time($n->date,false);?></span>
							<div class="blog_title"><a href="<?=base_url()?>news/<?=$n->slug;?>"><?=$n->title;?></a></div>
							<div class="blog_textarea">
								<?=smart_trim($n->content,100);?>
							</div>
							<a href="<?=base_url()?>news/<?=$n->slug;?>" class="btn_df btn_dafault">read more</a>
						</div>
					</div>
				</div>	
			<?endforeach;?>
		</div>
	</div>
</section>
<!--================= End Blog Section ============-->