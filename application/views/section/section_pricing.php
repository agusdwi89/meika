<? $items = $this->db->get_where('section_pricing_items',array('pricing_id'=>$data->id));?>
<!--============== Start Price Section ================-->
<section id="pricing" class="py_80" data-anchor="our_price">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="section_title wow animated slideInUp">
					<h2><?=$data->title;?></h2>
					<p><?=$data->description;?></p>
				</div>
			</div>
			<?foreach ($items->result() as $p): ?>
				<?$lists = array_filter(json_decode($p->lists));?>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="price_table wow animated slideInLeft">
						<div class="pricing_img">
							<img src="<?=base_url()?>assets/section/<?=$p->image;?>" alt="Price Image">
						</div>
						<div class="price"><?=$p->price;?></div>
						<div class="Price_category"><?=$p->name;?></div>
						<div class="price_list">
							<ul>
								<?foreach ($lists as $l): ?>
									<li><?=$l;?></li>	
								<?endforeach;?>
							</ul>
						</div>
					</div>
				</div>	
			<?endforeach;?>
		</div>
	</div>
</section>
<!--=============== End Our Price Section =============-->