<?
	$ubuy	= $this->db->get_where('site_config',array('id'=>9))->row()->value;
	$useeb	= $this->db->get_where('site_config',array('id'=>10))->row()->value;
	$useet	= $this->db->get_where('site_config',array('id'=>11))->row()->value;

	$ubprd = $this->db->query("select prd_id,count(prd_id) as c FROM orders GROUP BY prd_id");
	$ubprda = array();
	foreach ($ubprd->result() as $u) {
		$ubprda[$u->prd_id] = $u->c;
	}
?>

<style type="text/css">
.buy-soldout{
	color: #ffffff;
	background: #ce3a3a !important;
}
.buy-soldout:hover{
	color: #ffffff;
	background-color: #8a2e2e !important;
}
.buy-soldout:before{
    background-color: #8a2e2e !important;
}

</style>

<?$items = $this->db->get_where('section_product_items',array('prd_id'=>$data->id,'active'=>'yes'));?>
<?if ($data->type == 'type_1' || $data->type == 'type_2'): ?>
<section id="product" class="products block gray">
	<div class="container">
		<div class="row">
			<h2><?=$data->title;?></h2>
			<div class="block-desc"><p><?=$data->description;?></p></div>
			<?
				$item = $items->num_rows();
				$totbaris = (intdiv(($item-1),3)+1);
				$highlight = 3 * ($totbaris-1) + 1;
				
				$offset = 0;
				$sisa = ($item % 3);
				if ($sisa > 0 ){
					$offset = 1 / $sisa * 4;
				}
				
			?>
				<?$i=0;foreach ($items->result() as $p):$i++;?>			
			<?
				$offstring = "";
				if(($offset > 0) && ($i == $highlight)){
					$offstring = "col-md-offset-$offset";
				}
			?>
				<div id="product-1-i" class="col-md-4 product <?=$offstring?>">
					<div class="product-block">
						<div class="product-bg">
							<?if ($data->type == 'type_2'): ?>
								<div style="background: url(<?=base_url()?>assets/section/<?=$p->image_bg;?>) center no-repeat !important;" class="product-bg-img"></div>
								<div class="product-bg-overlay"></div>
							<?endif;?>
						</div>
						<div class="product-content">
							<h3><?=$p->title;?></h3>
							<p <?=(($data->type == 'type_2') ? 'class="version-bg"' : '');?> ><?=$p->description;?></p>
							<?if ($p->price_dsc > 0): ?>
								<span class="sale">Rp <?=format_number($p->price);?></span>
								<span class="sale-price">Rp <?=format_number($p->price_dsc);?></span>
							<?else:?>
								<span>Rp <?=format_number($p->price);?></span>
							<?endif;?>
							<div class="product-img">
								<img src="<?=base_url()?>assets/section/<?=$p->image;?>" alt="<?=$p->title;?>">
							</div>
							<div class="bnt-block">
								<?php if ($p->available == "yes"): ?>
									<a href="#order" 
										data-id="<?=$p->id;?>" 
										data-image="<?=base_url()?>assets/section/<?=$p->image;?>" 
										data-title="<?=$p->title;?>" 
										data-description="<?=$p->description;?>" 
										data-description2="<?=$p->description2;?>" 
										data-price="<?=$p->price;?>" 
										data-price_dsc="<?=$p->price_dsc;?>" 
										class="button buy-button" title="Buy Now">Buy Now</a>
								<?php else: ?>
									<a class="button buy-soldout" title="SOLD OUT">SOLD OUT</a>
								<?php endif ?>
								<br>
								<br>
							</div>
						</div>
						<div class="bottom_prd">
							<div class="view">
								<span>sedang dipesan : <?=rand($useeb,$useet)?></span>
								<span><i data-icn="icon-eye" class="icon-eye active"></i></span>
							</div>
							<div class="sold">
								<span style="margin-left: 20px"><i data-icn="icon-basket-loaded" class="icon-basket-loaded active"></i></span>
								<span>terjual : <?=$ubprda[$p->id]+$ubuy?></span>
							</div>
						</div>
					</div>
				</div>	
			<?endforeach;?>
		</div>
	</div> 
</section>

<?else:?>
<section id="product" class="products-l2 block">
	<div class="container">
		<div class="row">
			<h2><?=$data->title;?></h2>
			<div class="block-desc"><p><?=$data->description;?></p></div>


			<?$i=0;foreach ($items->result() as $p):$i++;?>
			
			<div id="product-l2-1" class="col-md-6 col product-l2">
				<div class="product-l2-block">
					<div class="product-l2-bg">
						<div class="bg-img"></div>
						<div class="bg-content"></div>
					</div>
			
					<div class="product-l2-img">
						<div class="product-l2-container">
							<img src="<?=base_url()?>assets/section/<?=$p->image;?>" alt="<?=$p->title;?>">
						</div>
					</div>
			
					<div class="product-l2-content">
						<div class="product-info">
							<h3><?=$p->title;?></h3>
							<p class="version"><?=$p->description;?></p>

							<?if ($p->price_dsc > 0): ?>
								<span class="sale">Rp <?=format_number($p->price);?></span>
								<span class="sale-price">Rp <?=format_number($p->price_dsc);?></span>
							<?else:?>
								<span>Rp <?=format_number($p->price);?></span>
							<?endif;?>

							<p class="desc"><?=$p->description2;?></p>
							
							<div class="l2-bnt-block">
								<?php if ($p->available == "yes"): ?>
									<a href="#order" 
										data-image="<?=base_url()?>assets/section/<?=$p->image;?>" 
										data-title="<?=$p->title;?>" 
										data-description="<?=$p->description;?>" 
										data-description2="<?=$p->description2;?>" 
										data-price="<?=$p->price;?>" 
										data-price_dsc="<?=$p->price_dsc;?>" 
										class="button buy-button" title="Buy Now">Buy Now</a>
								<?php else: ?>
								<a class="button buy-soldout" title="SOLD OUT">SOLD OUT</a>
								<?php endif ?>
								<br>
								<br>
							</div>
						</div>
					</div>
					<div class="bottom_prd">
						<div class="view">
							<i data-icn="icon-eye" class="icon-eye active"></i>&nbsp;&nbsp;&nbsp;<?=rand($useeb,$useet)?>
						</div>
						<div class="sold">
							<i data-icn="icon-basket-loaded" class="icon-basket-loaded"></i>&nbsp;&nbsp;&nbsp;<?=$ubprda[$p->id]+$ubuy?>
						</div>
					</div>
				</div>
			</div>

			<?if ($i % 2 == 0): ?>
				<div class="clearfix"></div>
			<?endif;?>

			<?endforeach;?>

		</div>
	</div>
</section>
<?endif;?>