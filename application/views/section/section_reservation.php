<!--============= Start About Section ============-->
<section id="reservation" class="py_80" data-scroll="about_us">
	<div class="container">
	  	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			  <div class="section_title wow animated slideInUp">
				  <h2><?=$data->title;?></h2>
				  <p><?=$data->subtitle;?>.</p>
			  </div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="about_img wow animated fadeInLeft">
				  <img src="<?=base_url()?>assets/section/<?=$data->image;?>" alt="About us Image">
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="about_content wow animated fadeInRight">
				  <p><span>“ <?=$data->tagline;?> ”</span></p>
				  <p><?=$data->description;?></p>
				  	
				  	<?php if (strlen($data->button_text) > 0): ?>
					 	<?if ($data->link == 'internal'): ?>
							<a class="btn_df btn_dafault" href="#<?=$data->url;?>"><?=$data->button_text;?></a>				  
					  	<?else:?>
					  		<a class="btn_df btn_dafault" target="_blank" href="<?=$data->url;?>"><?=$data->button_text;?></a>
					 	<?endif;?>
					<?php endif ?>					 	

				 	<?php if (strlen($data->reservation_button_text) > 0): ?>
				 		<a class="btn_df btn_dafault" href="#resarvation_from" data-toggle="modal" data-target="#myModal"><?=$data->reservation_button_text;?></a>
				 	<?php endif ?>

				</div>
			</div>
	  	</div>
	</div>
</section>
<!--============= Edn About Section ==============-->



<!--=========== Start Resarvation From =========-->
<div id="resarvation_from">
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="container">
				<div class="row">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="section_title wow animated slideInUp">
									<h2><?=$data->form_title;?></h2>
									<p id="sub-title-reserv"><?=$data->form_description;?></p>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<?=form_open(base_url('home/reserv'),array('class'=>'reservation_form'))?>
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<h3>Treatment Details</h3>
											<div class="row">
												<div class="form-group col-md-12 col-sm-12">
													<label>Date of Arrival</label>
													<div data-name="date" class="bfh-datepicker"></div>
												</div>
												<div class="form-group col-md-12 col-sm-12">
													<label>Preferred Time</label>
													<div data-name="time" class="bfh-timepicker"></div>
												</div>
												<div class="form-group col-md-12 col-sm-12">
													<label>No Of Guest</label>
													<select name="guest">
														<option>1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
														<option>6</option>
														<option>7</option>
														<option>8</option>
														<option>9</option>
														<option>10</option>
													</select>
												</div>
												<div class="form-group col-md-12 col-sm-12">
													<label>Type Of Treatment</label>
													<select name="treatment">
														<option>Full Body Massage</option>
														<option>spa treatment</option>
														<option>skin care treatment</option>
														<option>beauty</option>
														<option>nail treatment</option>
														<option>Hair Treatment</option>
													</select>
												</div>
												<div class="col-md-12 col-sm-12">
													<strong><?=$data->form_footer_main;?> <span><?=$data->form_footer_secondary;?></span></strong>
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<h3>Guest Details</h3>
											<div class="row">
												<div class="form-group col-md-12 col-sm-12">
													<label>name</label>
													<input class="form-control no-border" id="name" type="text" name="name" placeholder="Enter Your Name"/>
												</div>
												<div class="form-group col-md-12 col-sm-12">
													<label>email</label>
													<input class="form-control" id="email" type="text" name="email" placeholder="Enter Your Email"/>
												</div>
												<div class="form-group col-md-12 col-sm-12">
													<label>Phone</label>
													<input class="form-control" id="number" type="text" name="number" placeholder="Enter Your Phone Number"/>
												</div>
												<div class="form-group col-md-12 col-sm-12">
													<label>Address</label>
													<input class="form-control" id="Address" type="text" name="address" placeholder="Enter Your Address"/>
												</div>
												<div class="form-group col-md-12 col-sm-12">
													<a class="btn_df btn_dafault submit_reserv" href="#">Submit Your Request</a>
												</div>
											</div>
										</div>
									</div>
								<?=form_close()?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--============== End Resarvation From ===============-->