<?$items = $this->db->get_where('section_review_items',array('srev_id'=>$data->id));?>
<!--================= Start Our Client Section ====-->
<section id="review" class="py_80" data-anchor="consumer">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="section_title wow animated slideInUp">
					<h2><?=$data->title;?></h2>
					<p><?=$data->description;?></p>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="testimonials-carousel">
					<?foreach ($items->result() as $it): ?>
						<div class="item">
							<div class="client">
								<div class="avater">
									<img src="<?=base_url()?>assets/section/<?=$it->image;?>" alt="client image 1">
								</div>
								<div class="client_name">
									<h6><?=$it->name;?></h6>
								</div>
								<div class="feedback">
									<p><?=$it->review;?></p>
								</div>
							</div>
						</div>	
					<?endforeach;?>
				</div>
			</div>
		</div>
	</div>
</section>
<!--============== End Our Client Section ================-->