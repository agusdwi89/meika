<? $items = $this->db->get_where('section_services_item',array('services_id'=>$data->id));?>
<!--============= Start Services Section =========-->
<section id="services" class="py_80" data-anchor="services">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="section_title wow animated slideInUp">
					<h2><?=$data->title;?></h2>
					<p><?=$data->subtitle;?></p>
				</div>
			</div>

			<?foreach ($items->result() as $v): ?>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="service_widget wow animated slideInUp">
						<div class="service_img">
							<img src="<?=base_url()?>assets/section/<?=$v->image;?>" alt="bodymassage">
							<div class="serimg_overlay"></div>
						</div>
						<div class="service_iconprice">
							<div class="ser_icon">
								<span class="<?=$v->icon;?>"></span>
							</div>
						</div>
						<div class="service_content">
							<h3><?=$v->title;?></h3>
							<p><?=$v->description;?></p>
						</div>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
</section>
<!--============== End Services Section ==============-->