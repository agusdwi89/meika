<?$items = $this->db->get_where('section_slider_items',array('slider_id'=>$data->id));?>

<!--=============== Start Slider Section ================-->

<section id="slider"> 
	<div class="container-fluid">
		<div class="row">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">

					<?$i=0;foreach ($items->result() as $r): $i++;?>
						<div class="item <?=(($i==1) ? 'active' : '');?>">
							<div class="overlay"></div>
							<img src="<?=base_url()?>assets/section/<?=$r->image;?>" alt="Slider Image 1">
							<div class="carousel-caption">
								<div class="slider-title">
									<h1><?=$r->title1;?> <span><?=$r->title2;?></span></h1>
								</div>
								<p><?=$r->description;?></p>
								<?php if (strlen($r->button_text) > 0) : ?>
									<div class="btn-slid">
										<?if ($r->link == "internal"): ?>
											<a class="btn_df btn_dafault" href="<?=substr(base_url(),0,-1)?>#<?=$r->url;?>"><?=$r->button_text;?></a>
										<?else:?>
											<a class="btn_df btn_dafault" target="_blank" href="<?=$r->url;?>"><?=$r->button_text;?></a>
										<?endif;?>
									</div>
								<?php endif ?>
							</div>
						</div>
					<?endforeach;?>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="bdr bg-hvr txt fa fa-angle-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="bdr bg-hvr txt fa fa-angle-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> 
			</div>
		</div>
	</div>
</section>

<!--============= End Slider Section =============-->


