<?$items = $this->db->get_where('section_team_items',array('steam_id'=>$data->id));	?>
<!--============== Start Our Team Section =================-->
<section id="team" class="py_80" data-anchor="our_team">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="section_title wow animated slideInUp">
					<h2><?=$data->title;?></h2>
					<p><?=$data->description;?></p>
				</div>
			</div>

			<?foreach ($items->result() as $d): ?>
				<?$social = json_decode($d->social);?>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="team_widget wow animated slideInLeft">
						<div class="team_image">
							<img src="<?=base_url()?>assets/section/<?=$d->photo;?>" alt="Team image 1">
							<div class="teamimg_overlay"></div>
							<div class="member_data">
								<h4><?=$d->name;?></h4>
								<span><?=$d->position;?></span>
								<div class="socal_icon">
									<ul>
										<?foreach ($social as $key => $value): ?>
											<li><a href="<?=$value;?>"><i class="fa fa-<?=$key;?>"></i></a></li>	
										<?endforeach;?>
									</ul>
								</div>
							</div>
						</div>
						<div class="member_profile">
							<div class="member_data">
								<h4><?=$d->name;?></h4>
								<span><?=$d->position;?></span>
								<div class="socal_icon">
									<ul>
										<?foreach ($social as $key => $value): ?>
											<li><a href="<?=$value;?>"><i class="fa fa-<?=$key;?>"></i></a></li>	
										<?endforeach;?>
									</ul>
								</div>
							</div>
							<div class="member_content">
								<p><?=$d->description;?></p>
							</div>
						</div>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
</section>
<!--============== End Our Team Section =================-->