<!--============== Start Our Video Section ===========-->
<div id="video" class="py_80 spa_video" style="background-image: url(<?=base_url()?>assets/section/<?=$data->image;?>">
	<div class="container-fluid">
		<div class="row">
		  <div class="video wow animated fadeIn">
			<h4><?=$data->title;?></h4>
			<a class="bla-2" href="https://www.youtube.com/watch?v=<?=$data->link;?>">
			  <span class="flaticon-play-button"></span>
			</a> 
		  </div>
		</div>
	</div>
</div>
<!--============ End Our Video Section ================-->