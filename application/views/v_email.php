<div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;font-size:14px;color:#404040;margin:0;padding:0;max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
	<table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px" bgcolor="transparent">
		<tbody>
			<tr style="margin:0;padding:0">
				<td>
					<h2>MEIKACLINIC.COM</h2>
				</td>
			</tr>
			<tr>
				<td style="text-align:center">
					<br>
				</td>
			</tr>
			<?if ($notif): ?>
			<tr style="margin:0;padding:0">
				<td style="margin:0;padding:0" colspan="2">
					<b style="line-height:32px;color:#666;font-weight:500;font-size:16px;margin:0 0 16px;padding:0">
						Notifikasi reservasi online
					</b>
					<p style="font-weight:500;color:#666;font-size:14px;line-height:1.6;margin:0;padding:0">
						Berikut ringkasan reservasi online customer :
						<span style="color:#ff5722">
						</span>
					</p>
				</td>
			</tr>
			<?else:?>
			<tr style="margin:0;padding:0">
				<td style="margin:0;padding:0" colspan="2">
					<b style="line-height:32px;color:#666;font-weight:500;font-size:16px;margin:0 0 16px;padding:0">
						Terimakasih telah melakukan reservasi online di meikaclinic.com
					</b>
					<p style="font-weight:500;color:#666;font-size:14px;line-height:1.6;margin:0;padding:0">
						Berikut ringkasan reservasi anda :
						<span style="color:#ff5722">
						</span>
					</p>
				</td>
			</tr>
			<?endif;?>
			<tr style="margin:0;padding:0">
				<td style="margin:0;padding:0" colspan="2">
				</td>
			</tr>
		</tbody>
	</table>
	<table style="width:100%;margin-bottom:24px;padding:0 20px">
		<thead>
			<tr>
				<td style="width:50%">
					<span style="font-size:12px;color:#999;font-weight:normal;word-break:break-word">Tanggal Visit</span>
				</td>
				<td style="width:50%">
					<span style="font-size:12px;color:#999;font-weight:normal;word-break:break-word">Waktu Visit</span>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="vertical-align:top">
					<span style="font-size:20px;color:#666;font-weight:500;word-break:break-word"><?=pretty_date($date,false);?></span>
				</td>
				<td style="vertical-align:top">
					<span style="font-size:20px;color:#666;font-weight:500;word-break:break-word"><?=$time;?></span>
				</td>
			</tr>
		</tbody>
	</table>
	<table style="width:100%;margin-bottom:24px;padding:0 20px">
		<thead>
			<tr>
				<td style="width:50%">
					<span style="font-size:12px;color:#999;font-weight:normal;word-break:break-word">Jumlah Tamu</span>
				</td>
				<td style="width:50%">
					<span style="font-size:12px;color:#999;font-weight:normal;word-break:break-word">Treatment</span>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="vertical-align:top">
					<span style="font-size:20px;color:#666;font-weight:500;word-break:break-word"><?=$guest;?> orang</span>
				</td>
				<td style="vertical-align:top">
					<span style="font-size:20px;color:#666;font-weight:500;word-break:break-word"><?=$treatment;?></span>
				</td>
			</tr>
		</tbody>
	</table>

	<div style="padding:0 0 0 20px">
		<div style="border-top:1px solid #e0e0e0;padding:8px 0">
			<h2 style="font-size:14px;font-weight:normal;color:#666">Identitas Pemesan</h2>
			<span style="width:24px;border:2px solid #ff5722;display:inline-block;margin-left:0"></span>
		</div>

		<table style="width:100%;max-width:600px;padding-bottom:18px;padding:10px 20px 10px 0;border-bottom:1px solid #e0e0e0">
			<tbody>
				<tr style="margin:0">
					<td style="vertical-align:top">
						<p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:normal;width:120px">Nama</p>
					</td>
					<td style="vertical-align:top;">
						<b><?=$name;?></b>
					</td>
				</tr>
				<tr style="margin:0">
					<td style="vertical-align:top">
						<p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:normal;width:120px">Email</p>
					</td>
					<td style="vertical-align:top;">
						<b><?=$email;?></b>
					</td>
				</tr>
				<tr style="margin:0">
					<td style="vertical-align:top">
						<p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:normal;width:120px">Telp</p>
					</td>
					<td style="vertical-align:top;">
						<b><?=$number;?></b>
					</td>
				</tr>
				<tr style="margin:0">
					<td style="vertical-align:top">
						<p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:normal;width:120px">Alamat</p>
					</td>
					<td style="vertical-align:top;">
						<b><?=$address;?></b>
					</td>
				</tr>
			</tbody>
		</table>

	</div>

	<div style="padding:0 20px">
		<p style="font-size:14px;color:#999;padding:16px 0;margin:0;border-top:1px solid #e0e0e0">
			Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
		</p>
	</div>


	<table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;clear:both!important;background-color:transparent;margin:0 0 10px;padding:0" bgcolor="transparent">
		<tbody>

			<tr style="margin:0;padding:0">
				<td style="margin:0;padding:0"></td>
				<td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0px">
				</td>
			</tr>
		</tbody>
	</table>
	<table align="center">
		<tbody><tr style="margin:0;padding:0 0 0 0">
			<td style="display:block!important;width:600px!important;clear:both!important;margin:0 auto;padding:0">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;background-color:#f7f7f7;font-size:13px;color:#999999;border-top:1px solid #dddddd">
					<tbody><tr>
						<td width="600" align="center" style="padding:30px 20px 0"><?=$this->db->get_where('site_config',array('id'=>8))->row()->value;?></td>
					</tr>
					<tr>
						<td width="600" align="center" style="padding:10px 20px 30px">
							&copy; 2019, <span class="il">meikaclinic.com</span>
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
	</tbody>
</table>
</div>