<!DOCTYPE html>
<html>
<head>
	<?=$this->load->view('qs/header');?>
	<script type="text/javascript">
		var BASE_URL = "<?=base_url()?>";
		var CLOG = {};
	</script>
</head>
<body id="page_top" class="page-wrapper page-load" data-anchor="page_top">
	<?=form_open('/',array('id'=>'global-form'))?><?=form_close()?>

	<a href="#" id="scroll" style="display: none;"><span></span></a>
	<!--=========== Start Page Loader ==============-->
	<div class="preloader">
		<div id="loader-wrap" class="loader-wrap">
			<div class="cssload-loader">
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
			</div>
		</div>
	</div>
	<!--=========== End Page Loader ===============-->
	
	<?=$this->load->view('fe/nav');?>

	<?//debug_array($list_section);?>

	<!--=========== Start Header Nav ===============-->
	
	<?php foreach ($list_section as $key): ?>

		<?=$this->load->view('section/'.$key['layout'],$key);?>

	<?php endforeach ?>

	<?=$this->load->view('qs/footerscript');?>
	<?=$this->load->view('fe/purechat');?>

</body>
</html>