<!DOCTYPE html>
<html>
<head>
	<!-- Meta Tag -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?=$item->meta_description;?>">
	<meta name="keywords" content="<?=$item->meta_keyword;?>">
	<meta name="author" content="meika">
	<title><?=$item->title;?></title>

	<!-- Links of CSS Files -->
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/settings.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/style.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/responsive.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/color.css" id="color-change">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/default-animate.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/font-awesome.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/fonts/flaticon.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/bootstrap-formhelpers.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/qs/css/loader.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/fe/css/simple-line-icons.css">


	<script type="text/javascript">
		var BASE_URL = "<?=base_url()?>";
		var CLOG = {};
	</script>
</head>
<body id="page_top" class="page-wrapper page-load" data-anchor="page_top">
	<?=form_open('/',array('id'=>'global-form'))?><?=form_close()?>

	<a href="#" id="scroll" style="display: none;"><span></span></a>
	<!--=========== Start Page Loader ==============-->
	<div class="preloader">
		<div id="loader-wrap" class="loader-wrap">
			<div class="cssload-loader">
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
				<div class="cssload-side"></div>
			</div>
		</div>
	</div>
	
	<?
		$bg_image = "";
		$imgs = $this->db->order_by('id','desc')->limit(1)->get('section_news')->row()->image;
	?>

	<section id="Bannar" class="bgoverlay" style="background-image: url(<?=base_url()?>assets/section/<?=$imgs;?>);">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="bannar_area">
						<h2>Meika News</h2>
						<ul class="banner_nav">
							<li class="active">Back to </li>
							<li >
								<a href="<?=base_url()?>">Home</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div> 
	</section>

	<section id="blogpost">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="single_post wow animated slideInUp animated" style="visibility: visible; animation-name: slideInUp;">
						<div class="blog_image"> <img src="<?=base_url()?>assets/news/<?=$item->img_header;?>" alt="Image"></div>
						<div class="blog_data">
							<div class="blog_title">
								<a href="<?=base_url()?>news/<?=$item->slug;?>"><?=$item->title;?></a>
							</div>
							<div class="blog_info"> 
								<span class="blogdate"><?=format_date_time($item->date,false);?></span> 
								<span class="admin"><i class="fa fa-user"></i><a href="#">Admin</a>
							</div>
							<div class="blog_textarea">
								<?=$item->content;?>
							</div>
							<div class="share_icon">
								<ul>
									<li>Share :</li>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-rss"></i></a></li>
								</ul>
							</div>				
							<div>
								<br><br>
								<center><a class="btn_df btn_dafault" href="<?=base_url()?>">Back to home</a></center>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
	</section>

	<?
		$footer_id 		= $this->db->limit(1)->get_where('list_section',array('type_sec_id'=>14))->row()->section_id;
		$footer_text 	= $this->db->get_where('section_footer',array('id'=>$footer_id))->row()->footer_text;
	?>

	<footer id="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="coppyright_area">
						<?=$footer_text?>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<?=$this->load->view('qs/footerscript');?>
	<?=$this->load->view('fe/purechat');?>
</body>
</html>