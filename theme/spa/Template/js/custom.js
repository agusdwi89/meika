/*==================================================================================

* Template Name: Beautyqueen - Spa and Beauty Salon HTML5 Template
* Version: 1.0.5
* Author: themetrading
* Email: themetrading@gmail.com
* Developed By: themetrading
* First Release: 30 September 2018
* Author URL: www.themetrading.com

=====================================================================================*/
//  01. Youtube and Vimeo video popup control
// 	02. Preloader For Hide loader
// 	03.	Testimonials Carousel Slider 
// 	04. Smoothscroll js
// 	05. navbar scrolling logo change
// 	06. Elements Wow Animation
//	07.	LightBox / Fancybox
// 	08. Pretty Photo
// 	09. When document is loading, do
//	10.	Gallery With Filters List	
// 	11.	Scroll Top
//	12.	Contact Form Validation
/*=====================================================================================*/	

(function ($) {
    "use strict";

var $body     = $("body"),
	$window   = $(window),
	$contact  = $('#contact-form')
	  
$body.scrollspy({
	target: ".navbar-collapse",
	offset: 20

});
/*===================================================================================
// 01. Youtube and Vimeo video popup control
=====================================================================================*/

$(function(){
	$("a.bla-2").YouTubePopUp(); // Disable autoplay
});
	
/*====================================================================
// 02. Preloader For Hide loader
======================================================================*/

function handlePreloader() {
	if($('.preloader').length){
		$('.preloader').delay(500).fadeOut(500);
		$('body').removeClass('page-load');
	}
}

/*======================================================================
// 03.	Testimonials Carousel Slider 
========================================================================*/		
	$('.testimonials-carousel').owlCarousel({
	 loop: true,
	 autoplay: true,
	 autoplayTimeout: 1500,
	 margin: 30,
	 nav: false,
	 dots: true,
	 responsive:{

			0:{
				items:1
			},
			600:{
				items:1
			},
			1024:{
				items:1
			},
			1200:{
				items:1
			}
		}
		
	 });
/*==========================================================================		
// 04. Smoothscroll js
============================================================================*/

$("a").on('click', function(event) {

    if (this.hash !== "") {
      event.preventDefault();
	  
      var hash = this.hash;

      $('html, body').animate({
        scrollTop: $(hash).offset().top 
      }, 1000, function(){
   
        window.location.hash = hash;
      });
    }
  });
  
/*===============================================================================
// 05. navbar scrolling logo change
=================================================================================*/

$window.on("scroll",function () {

	var bodyScroll = $window.scrollTop(),
		navbar = $(".navbar"),
		logo = $(".navbar .navbar-brand> img");

	if(bodyScroll > 100){

		navbar.addClass("nav-scroll");
		logo.attr('src', 'images/logo/logo-black.png');

	}else{

		navbar.removeClass("nav-scroll");
		logo.attr('src', 'images/logo/logo.png');
	}
});
		
/*=====================================================================================
// 06. Elements Wow Animation
=======================================================================================*/

if($('.wow').length){
	var wow = new WOW(
	  {
		boxClass:     'wow',     
		animateClass: 'animated', 
		offset:       0,          
		mobile:       true,       
		live:         true 
	  }
	);
	wow.init();
}

/*======================================================================================
//	07.	LightBox / Fancybox
========================================================================================*/

$('[data-fancybox="gallery"]').fancybox({
	 animationEffect: "zoom-in-out",
	 transitionEffect: "slide",
	 transitionEffect: "slide",
});

/*===========================================================================================
// 08.  Pretty Photo
=============================================================================================*/

$("a[rel^='prettyPhoto']").prettyPhoto({
	social_tools: false
});

$("formtoggle").click(function(){
	$(".resarvation").slideToggle("slow");
});

/*===========================================================================================
// 09. When document is loading, do	
=============================================================================================*/

$window.on('load', function() {
	handlePreloader();
});

/*============================================================================================		
//10.	Gallery With Filters List
==============================================================================================*/

if($('.filter-list').length){
	$('.filter-list').mixItUp({});
}

/*=============================================================================================
// 11.	Scroll Top
===============================================================================================*/

$(window).scroll(function(){ 
	if ($(this).scrollTop() > 500) { 
		$('#scroll').fadeIn(); 
	} else { 
		$('#scroll').fadeOut(); 
	} 
}); 
$('#scroll').click(function(){ 
	$("html, body").animate({ scrollTop: 0 }, 1000); 
	return false; 
}); 
/*================================================================================================
//	12.	Contact Form Validation
==================================================================================================*/
if($contact.length){
		$contact.validate({  //#contact-form contact form id
			rules: {
				name: {
					required: true    // Field name here
				},
				email: {
					required: true, // Field name here
					email: true
				},
				subject: {
					required: true
				},
				message: {
					required: true
				}
			},
			
			messages: {
                name: "Please enter your First Name", //Write here your error message that you want to show in contact form
                email: "Please enter valid Email", //Write here your error message that you want to show in contact form
                subject: "Please enter your Subject", //Write here your error message that you want to show in contact form
				message: "Please write your Message" //Write here your error message that you want to show in contact form
            },

            submitHandler: function (form) {
                $('#send').attr({'disabled' : 'true', 'value' : 'Sending...' });
                $.ajax({
                    type: "POST",
                    url: "email.php",
                    data: $(form).serialize(),
                    success: function () {
                        $('#send').removeAttr('disabled').attr('value', 'Send');
                        $( "#success").slideDown( "slow" );
                        setTimeout(function() {
                        $( "#success").slideUp( "slow" );
                        }, 5000);
                        form.reset();
                    },
                    error: function() {
                        $('#send').removeAttr('disabled').attr('value', 'Send');
                        $( "#error").slideDown( "slow" );
                        setTimeout(function() {
                        $( "#error").slideUp( "slow" );
                        }, 5000);
                    }
                });
                return false; // required to block normal submit since you used ajax
            }

		});
	}
	
})(jQuery);